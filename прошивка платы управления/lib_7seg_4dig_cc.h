/*
  Библиотека для управления 7-сегментным индикатором с общим катодом
  катоды подключены через транзисторы на землю, то есть тоже управляются
  высоким уровнем
  
  Сконфигурировать нужно пины подключения и функцию инициализации
*/

#include "bits4lib.h"

#define seg_A PORTC_bits.bit3
#define seg_B PORTC_bits.bit4
#define seg_C PORTC_bits.bit1
#define seg_D PORTC_bits.bit2
#define seg_E PORTD_bits.bit2
#define seg_F PORTD_bits.bit1
#define seg_G PORTD_bits.bit4
#define seg_dp PORTD_bits.bit3
#define seg_d1 PORTB_bits.bit6
#define seg_d2 PORTD_bits.bit0
#define seg_d3 PORTC_bits.bit5
#define seg_d4 PORTC_bits.bit0


void init_lib_7seg_4dig_cc(void);

void init_lib_7seg_4dig_cc(void) {
  // инициализируем порты на выход
  DDRB |= 0b01000000;
  DDRC |= 0b00111111;
  DDRD |= 0b00011111;
  
  // инициализируем таймер развертки
  TCCR0 = (0<<CS02)|(1<<CS01)|(0<<CS00); // делитель
  TIMSK |= (1<<TOIE0); // прерываение по переполнению
  TCNT0 = 0; // начальное значение таймера       
  sei(); // глобальное разрешение прерываний               
}

////////////////////////////////////////////////////////////////////////////

// показывает номер текущей цифры в развертке
extern int volatile lib_7seg_4dig__cur_dig_num = 0;
extern int volatile lib_7seg_4dig__display = 32;

ISR( TIMER0_OVF_vect )
{
  TCNT0 = 0; 
  
  
  lib_7seg_4dig__cur_dig_num++;
  if (lib_7seg_4dig__cur_dig_num == 4) {lib_7seg_4dig__cur_dig_num = 0;}
  
  char razr_4 = lib_7seg_4dig__display % 10;
  char razr_3 = (lib_7seg_4dig__display % 100) / 10;
  char razr_2 = (lib_7seg_4dig__display % 1000) / 100;
  char razr_1 = (lib_7seg_4dig__display) / 1000;
  
  
  
  if (lib_7seg_4dig__cur_dig_num == 0) {
    lib_7seg_4dig__set_char(razr_1);
  }
  if (lib_7seg_4dig__cur_dig_num == 1) {
    lib_7seg_4dig__set_char(razr_2);
  }
  if (lib_7seg_4dig__cur_dig_num == 2) {
    lib_7seg_4dig__set_char(razr_3);
  }
  if (lib_7seg_4dig__cur_dig_num == 3) {
    lib_7seg_4dig__set_char(razr_4);
  }
  
  lib_7seg_4dig__set_digit();
  //lib_7seg_4dig__dot_enable();
  
  
 
 
  //seg_d1 = 1;
  //seg_d2 = 1;
    //seg_d3 = 1;
    //seg_d4 = 1;
    
   
}

void lib_7seg_4dig__set_char(char a_char);
void lib_7seg_4dig__dot_enable();
void lib_7seg_4dig__set_digit();

void lib_7seg_4dig__set_char(char a_char) {
  if (a_char == 0) {
    seg_A = 1; seg_B = 1; seg_C = 1; seg_D = 1;
    seg_E = 1; seg_F = 1; seg_G = 0; seg_dp = 0;
  }
  if (a_char == 1) {
    seg_A = 0; seg_B = 1; seg_C = 1; seg_D = 0;
    seg_E = 0; seg_F = 0; seg_G = 0; seg_dp = 0;
  }
  if (a_char == 2) {
    seg_A = 1; seg_B = 1; seg_C = 0; seg_D = 1;
    seg_E = 1; seg_F = 0; seg_G = 1; seg_dp = 0;
  }
  if (a_char == 3) {
    seg_A = 1; seg_B = 1; seg_C = 1; seg_D = 1;
    seg_E = 0; seg_F = 0; seg_G = 1; seg_dp = 0;
  }
  if (a_char == 4) {
    seg_A = 0; seg_B = 1; seg_C = 1; seg_D = 0;
    seg_E = 0; seg_F = 1; seg_G = 1; seg_dp = 0;
  }
  if (a_char == 5) {
    seg_A = 1; seg_B = 0; seg_C = 1; seg_D = 1;
    seg_E = 0; seg_F = 1; seg_G = 1; seg_dp = 0;
  }
  if (a_char == 6) {
    seg_A = 1; seg_B = 0; seg_C = 1; seg_D = 1;
    seg_E = 1; seg_F = 1; seg_G = 1; seg_dp = 0;
  }
  if (a_char == 7) {
    seg_A = 1; seg_B = 1; seg_C = 1; seg_D = 0;
    seg_E = 0; seg_F = 0; seg_G = 0; seg_dp = 0;
  }
  if (a_char == 8) {
    seg_A = 1; seg_B = 1; seg_C = 1; seg_D = 1;
    seg_E = 1; seg_F = 1; seg_G = 1; seg_dp = 0;
  }
  if (a_char == 9) {
    seg_A = 1; seg_B = 1; seg_C = 1; seg_D = 1;
    seg_E = 0; seg_F = 1; seg_G = 1; seg_dp = 0;
  }
  if (a_char == 'o') {
    seg_A = 1; seg_B = 1; seg_C = 0; seg_D = 0;
    seg_E = 0; seg_F = 1; seg_G = 1; seg_dp = 0;
  }
}

void lib_7seg_4dig__dot_enable() {
  seg_dp = 1;
}

void lib_7seg_4dig__set_digit() {
  if (lib_7seg_4dig__cur_dig_num == 0) {
    seg_d1 = 1;
    seg_d2 = 0;
    seg_d3 = 0;
    seg_d4 = 0;
  }
  if (lib_7seg_4dig__cur_dig_num == 1) {
    seg_d1 = 0;
    seg_d2 = 1;
    seg_d3 = 0;
    seg_d4 = 0;
  }
  if (lib_7seg_4dig__cur_dig_num == 2) {
    seg_d1 = 0;
    seg_d2 = 0;
    seg_d3 = 1;
    seg_d4 = 0;
  }
  if (lib_7seg_4dig__cur_dig_num == 3) {
    seg_d1 = 0;
    seg_d2 = 0;
    seg_d3 = 0;
    seg_d4 = 1;
  }
}

