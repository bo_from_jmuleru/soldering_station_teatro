# 1 "main.c"
# 1 "/home/max/kicad/soldering_station_teatro/soldering_station_teatro/прошивка платы управления/with_m4/lib_7seg4dig_cc//"
# 1 "<command-line>"
# 1 "main.c"
# 1 "/usr/lib/avr/include/avr/io.h" 1 3
# 99 "/usr/lib/avr/include/avr/io.h" 3
# 1 "/usr/lib/avr/include/avr/sfr_defs.h" 1 3
# 126 "/usr/lib/avr/include/avr/sfr_defs.h" 3
# 1 "/usr/lib/avr/include/inttypes.h" 1 3
# 37 "/usr/lib/avr/include/inttypes.h" 3
# 1 "/usr/lib/gcc/avr/4.8.1/include/stdint.h" 1 3 4
# 9 "/usr/lib/gcc/avr/4.8.1/include/stdint.h" 3 4
# 1 "/usr/lib/avr/include/stdint.h" 1 3 4
# 121 "/usr/lib/avr/include/stdint.h" 3 4
typedef signed int int8_t __attribute__((__mode__(__QI__)));
typedef unsigned int uint8_t __attribute__((__mode__(__QI__)));
typedef signed int int16_t __attribute__ ((__mode__ (__HI__)));
typedef unsigned int uint16_t __attribute__ ((__mode__ (__HI__)));
typedef signed int int32_t __attribute__ ((__mode__ (__SI__)));
typedef unsigned int uint32_t __attribute__ ((__mode__ (__SI__)));

typedef signed int int64_t __attribute__((__mode__(__DI__)));
typedef unsigned int uint64_t __attribute__((__mode__(__DI__)));
# 142 "/usr/lib/avr/include/stdint.h" 3 4
typedef int16_t intptr_t;




typedef uint16_t uintptr_t;
# 159 "/usr/lib/avr/include/stdint.h" 3 4
typedef int8_t int_least8_t;




typedef uint8_t uint_least8_t;




typedef int16_t int_least16_t;




typedef uint16_t uint_least16_t;




typedef int32_t int_least32_t;




typedef uint32_t uint_least32_t;







typedef int64_t int_least64_t;






typedef uint64_t uint_least64_t;
# 213 "/usr/lib/avr/include/stdint.h" 3 4
typedef int8_t int_fast8_t;




typedef uint8_t uint_fast8_t;




typedef int16_t int_fast16_t;




typedef uint16_t uint_fast16_t;




typedef int32_t int_fast32_t;




typedef uint32_t uint_fast32_t;







typedef int64_t int_fast64_t;






typedef uint64_t uint_fast64_t;
# 273 "/usr/lib/avr/include/stdint.h" 3 4
typedef int64_t intmax_t;




typedef uint64_t uintmax_t;
# 10 "/usr/lib/gcc/avr/4.8.1/include/stdint.h" 2 3 4
# 38 "/usr/lib/avr/include/inttypes.h" 2 3
# 77 "/usr/lib/avr/include/inttypes.h" 3
typedef int32_t int_farptr_t;



typedef uint32_t uint_farptr_t;
# 127 "/usr/lib/avr/include/avr/sfr_defs.h" 2 3
# 100 "/usr/lib/avr/include/avr/io.h" 2 3
# 348 "/usr/lib/avr/include/avr/io.h" 3
# 1 "/usr/lib/avr/include/avr/iom8.h" 1 3
# 636 "/usr/lib/avr/include/avr/iom8.h" 3
       
# 637 "/usr/lib/avr/include/avr/iom8.h" 3

       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
# 349 "/usr/lib/avr/include/avr/io.h" 2 3
# 610 "/usr/lib/avr/include/avr/io.h" 3
# 1 "/usr/lib/avr/include/avr/portpins.h" 1 3
# 611 "/usr/lib/avr/include/avr/io.h" 2 3

# 1 "/usr/lib/avr/include/avr/common.h" 1 3
# 613 "/usr/lib/avr/include/avr/io.h" 2 3

# 1 "/usr/lib/avr/include/avr/version.h" 1 3
# 615 "/usr/lib/avr/include/avr/io.h" 2 3






# 1 "/usr/lib/avr/include/avr/fuse.h" 1 3
# 248 "/usr/lib/avr/include/avr/fuse.h" 3
typedef struct
{
    unsigned char low;
    unsigned char high;
} __fuse_t;
# 622 "/usr/lib/avr/include/avr/io.h" 2 3


# 1 "/usr/lib/avr/include/avr/lock.h" 1 3
# 625 "/usr/lib/avr/include/avr/io.h" 2 3
# 2 "main.c" 2

# 1 "/usr/lib/avr/include/util/delay.h" 1 3
# 43 "/usr/lib/avr/include/util/delay.h" 3
# 1 "/usr/lib/avr/include/util/delay_basic.h" 1 3
# 40 "/usr/lib/avr/include/util/delay_basic.h" 3
static inline void _delay_loop_1(uint8_t __count) __attribute__((always_inline));
static inline void _delay_loop_2(uint16_t __count) __attribute__((always_inline));
# 80 "/usr/lib/avr/include/util/delay_basic.h" 3
void
_delay_loop_1(uint8_t __count)
{
 __asm__ volatile (
  "1: dec %0" "\n\t"
  "brne 1b"
  : "=r" (__count)
  : "0" (__count)
 );
}
# 102 "/usr/lib/avr/include/util/delay_basic.h" 3
void
_delay_loop_2(uint16_t __count)
{
 __asm__ volatile (
  "1: sbiw %0,1" "\n\t"
  "brne 1b"
  : "=w" (__count)
  : "0" (__count)
 );
}
# 44 "/usr/lib/avr/include/util/delay.h" 2 3
# 1 "/usr/lib/avr/include/math.h" 1 3
# 127 "/usr/lib/avr/include/math.h" 3
extern double cos(double __x) __attribute__((__const__));





extern double sin(double __x) __attribute__((__const__));





extern double tan(double __x) __attribute__((__const__));






extern double fabs(double __x) __attribute__((__const__));






extern double fmod(double __x, double __y) __attribute__((__const__));
# 168 "/usr/lib/avr/include/math.h" 3
extern double modf(double __x, double *__iptr);



extern float modff (float __x, float *__iptr);




extern double sqrt(double __x) __attribute__((__const__));





extern double cbrt(double __x) __attribute__((__const__));
# 194 "/usr/lib/avr/include/math.h" 3
extern double hypot (double __x, double __y) __attribute__((__const__));







extern double square(double __x) __attribute__((__const__));






extern double floor(double __x) __attribute__((__const__));






extern double ceil(double __x) __attribute__((__const__));
# 234 "/usr/lib/avr/include/math.h" 3
extern double frexp(double __x, int *__pexp);







extern double ldexp(double __x, int __exp) __attribute__((__const__));





extern double exp(double __x) __attribute__((__const__));





extern double cosh(double __x) __attribute__((__const__));





extern double sinh(double __x) __attribute__((__const__));





extern double tanh(double __x) __attribute__((__const__));







extern double acos(double __x) __attribute__((__const__));







extern double asin(double __x) __attribute__((__const__));






extern double atan(double __x) __attribute__((__const__));
# 298 "/usr/lib/avr/include/math.h" 3
extern double atan2(double __y, double __x) __attribute__((__const__));





extern double log(double __x) __attribute__((__const__));





extern double log10(double __x) __attribute__((__const__));





extern double pow(double __x, double __y) __attribute__((__const__));






extern int isnan(double __x) __attribute__((__const__));
# 333 "/usr/lib/avr/include/math.h" 3
extern int isinf(double __x) __attribute__((__const__));






__attribute__((__const__)) static inline int isfinite (double __x)
{
    unsigned char __exp;
    __asm__ (
 "mov	%0, %C1		\n\t"
 "lsl	%0		\n\t"
 "mov	%0, %D1		\n\t"
 "rol	%0		"
 : "=r" (__exp)
 : "r" (__x) );
    return __exp != 0xff;
}






__attribute__((__const__)) static inline double copysign (double __x, double __y)
{
    __asm__ (
 "bst	%D2, 7	\n\t"
 "bld	%D0, 7	"
 : "=r" (__x)
 : "0" (__x), "r" (__y) );
    return __x;
}
# 376 "/usr/lib/avr/include/math.h" 3
extern int signbit (double __x) __attribute__((__const__));






extern double fdim (double __x, double __y) __attribute__((__const__));
# 392 "/usr/lib/avr/include/math.h" 3
extern double fma (double __x, double __y, double __z) __attribute__((__const__));







extern double fmax (double __x, double __y) __attribute__((__const__));







extern double fmin (double __x, double __y) __attribute__((__const__));






extern double trunc (double __x) __attribute__((__const__));
# 426 "/usr/lib/avr/include/math.h" 3
extern double round (double __x) __attribute__((__const__));
# 439 "/usr/lib/avr/include/math.h" 3
extern long lround (double __x) __attribute__((__const__));
# 453 "/usr/lib/avr/include/math.h" 3
extern long lrint (double __x) __attribute__((__const__));
# 45 "/usr/lib/avr/include/util/delay.h" 2 3
# 84 "/usr/lib/avr/include/util/delay.h" 3
static inline void _delay_us(double __us) __attribute__((always_inline));
static inline void _delay_ms(double __ms) __attribute__((always_inline));
# 141 "/usr/lib/avr/include/util/delay.h" 3
void
_delay_ms(double __ms)
{
 double __tmp ;



 uint32_t __ticks_dc;
 extern void __builtin_avr_delay_cycles(unsigned long);
 __tmp = ((4000000UL) / 1e3) * __ms;
# 160 "/usr/lib/avr/include/util/delay.h" 3
  __ticks_dc = (uint32_t)(ceil(fabs(__tmp)));


 __builtin_avr_delay_cycles(__ticks_dc);
# 186 "/usr/lib/avr/include/util/delay.h" 3
}
# 223 "/usr/lib/avr/include/util/delay.h" 3
void
_delay_us(double __us)
{
 double __tmp ;



 uint32_t __ticks_dc;
 extern void __builtin_avr_delay_cycles(unsigned long);
 __tmp = ((4000000UL) / 1e6) * __us;
# 242 "/usr/lib/avr/include/util/delay.h" 3
  __ticks_dc = (uint32_t)(ceil(fabs(__tmp)));


 __builtin_avr_delay_cycles(__ticks_dc);
# 268 "/usr/lib/avr/include/util/delay.h" 3
}
# 4 "main.c" 2
# 1 "m4_avr.h" 1







































































































































# 5 "main.c" 2
# 1 "m4_atmega8_dip28.h" 1
























# 6 "main.c" 2
# 1 "lib_7seg4dig_cc.h" 1
# 13 "lib_7seg4dig_cc.h"


  
  
  
  
  
  
  
  
  
  
  
  
  
  
;

void func(void) {

  (*(volatile uint8_t *)((0x14) + 0x20)) |= ( 1 << 2 );
  (*(volatile uint8_t *)((0x14) + 0x20)) |= ( 1 << 3 );
  (*(volatile uint8_t *)((0x14) + 0x20)) |= ( 1 << 4 );
  (*(volatile uint8_t *)((0x14) + 0x20)) |= ( 1 << 5 );
  (*(volatile uint8_t *)((0x11) + 0x20)) |= ( 1 << 0 );
  (*(volatile uint8_t *)((0x11) + 0x20)) |= ( 1 << 1 );
  (*(volatile uint8_t *)((0x11) + 0x20)) |= ( 1 << 2 );
  (*(volatile uint8_t *)((0x11) + 0x20)) |= ( 1 << 3 );
  (*(volatile uint8_t *)((0x11) + 0x20)) |= ( 1 << 4 );
  (*(volatile uint8_t *)((0x11) + 0x20)) |= ( 1 << 5 );
  (*(volatile uint8_t *)((0x11) + 0x20)) |= ( 1 << 6 );
  (*(volatile uint8_t *)((0x11) + 0x20)) |= ( 1 << 7 );

  (*(volatile uint8_t *)((0x15) + 0x20)) &= 0b11111011;
  (*(volatile uint8_t *)((0x15) + 0x20)) &= 0b11110111;
  (*(volatile uint8_t *)((0x15) + 0x20)) &= 0b11101111;
  (*(volatile uint8_t *)((0x15) + 0x20)) &= 0b11011111;
  (*(volatile uint8_t *)((0x12) + 0x20)) &= 0b11111110;
  (*(volatile uint8_t *)((0x12) + 0x20)) &= 0b11111101;
  (*(volatile uint8_t *)((0x12) + 0x20)) &= 0b11111011;
  (*(volatile uint8_t *)((0x12) + 0x20)) &= 0b11110111;
  (*(volatile uint8_t *)((0x12) + 0x20)) &= 0b11101111;
  (*(volatile uint8_t *)((0x12) + 0x20)) &= 0b11011111;
  (*(volatile uint8_t *)((0x12) + 0x20)) &= 0b10111111;
  (*(volatile uint8_t *)((0x12) + 0x20)) &= 0b01111111;
}

void set_char(char a_char) {
  if (a_char == 0) {
    (*(volatile uint8_t *)((0x15) + 0x20)) |= ( 1 << 2 );
    (*(volatile uint8_t *)((0x15) + 0x20)) |= ( 1 << 3 );
    (*(volatile uint8_t *)((0x15) + 0x20)) |= ( 1 << 4 );
    (*(volatile uint8_t *)((0x15) + 0x20)) |= ( 1 << 5 );
    (*(volatile uint8_t *)((0x12) + 0x20)) |= ( 1 << 0 );
    (*(volatile uint8_t *)((0x12) + 0x20)) |= ( 1 << 1 );
    (*(volatile uint8_t *)((0x12) + 0x20)) &= 0b11111011;
    (*(volatile uint8_t *)((0x12) + 0x20)) &= 0b11110111;
  }
  if (a_char == 1) {
    (*(volatile uint8_t *)((0x15) + 0x20)) &= 0b11111011;
    (*(volatile uint8_t *)((0x15) + 0x20)) |= ( 1 << 3 );
    (*(volatile uint8_t *)((0x15) + 0x20)) |= ( 1 << 4 );
    (*(volatile uint8_t *)((0x15) + 0x20)) &= 0b11011111;
    (*(volatile uint8_t *)((0x12) + 0x20)) &= 0b11111110;
    (*(volatile uint8_t *)((0x12) + 0x20)) &= 0b11111101;
    (*(volatile uint8_t *)((0x12) + 0x20)) &= 0b11111011;
    (*(volatile uint8_t *)((0x12) + 0x20)) &= 0b11110111;
  }
  if (a_char == 2) {
    (*(volatile uint8_t *)((0x15) + 0x20)) |= ( 1 << 2 );
    (*(volatile uint8_t *)((0x15) + 0x20)) |= ( 1 << 3 );
    (*(volatile uint8_t *)((0x15) + 0x20)) &= 0b11101111;
    (*(volatile uint8_t *)((0x15) + 0x20)) |= ( 1 << 5 );
    (*(volatile uint8_t *)((0x12) + 0x20)) |= ( 1 << 0 );
    (*(volatile uint8_t *)((0x12) + 0x20)) &= 0b11111101;
    (*(volatile uint8_t *)((0x12) + 0x20)) |= ( 1 << 2 );
    (*(volatile uint8_t *)((0x12) + 0x20)) &= 0b11110111;
  }
  if (a_char == 3) {
    (*(volatile uint8_t *)((0x15) + 0x20)) |= ( 1 << 2 );
    (*(volatile uint8_t *)((0x15) + 0x20)) |= ( 1 << 3 );
    (*(volatile uint8_t *)((0x15) + 0x20)) |= ( 1 << 4 );
    (*(volatile uint8_t *)((0x15) + 0x20)) |= ( 1 << 5 );
    (*(volatile uint8_t *)((0x12) + 0x20)) &= 0b11111110;
    (*(volatile uint8_t *)((0x12) + 0x20)) &= 0b11111101;
    (*(volatile uint8_t *)((0x12) + 0x20)) |= ( 1 << 2 );
    (*(volatile uint8_t *)((0x12) + 0x20)) &= 0b11110111;
  }
  if (a_char == 4) {
    (*(volatile uint8_t *)((0x15) + 0x20)) &= 0b11111011;
    (*(volatile uint8_t *)((0x15) + 0x20)) |= ( 1 << 3 );
    (*(volatile uint8_t *)((0x15) + 0x20)) |= ( 1 << 4 );
    (*(volatile uint8_t *)((0x15) + 0x20)) &= 0b11011111;
    (*(volatile uint8_t *)((0x12) + 0x20)) &= 0b11111110;
    (*(volatile uint8_t *)((0x12) + 0x20)) |= ( 1 << 1 );
    (*(volatile uint8_t *)((0x12) + 0x20)) |= ( 1 << 2 );
    (*(volatile uint8_t *)((0x12) + 0x20)) &= 0b11110111;
  }
  if (a_char == 5) {
    (*(volatile uint8_t *)((0x15) + 0x20)) |= ( 1 << 2 );
    (*(volatile uint8_t *)((0x15) + 0x20)) &= 0b11110111;
    (*(volatile uint8_t *)((0x15) + 0x20)) |= ( 1 << 4 );
    (*(volatile uint8_t *)((0x15) + 0x20)) |= ( 1 << 5 );
    (*(volatile uint8_t *)((0x12) + 0x20)) &= 0b11111110;
    (*(volatile uint8_t *)((0x12) + 0x20)) |= ( 1 << 1 );
    (*(volatile uint8_t *)((0x12) + 0x20)) |= ( 1 << 2 );
    (*(volatile uint8_t *)((0x12) + 0x20)) &= 0b11110111;
  }
  if (a_char == 6) {
    (*(volatile uint8_t *)((0x15) + 0x20)) |= ( 1 << 2 );
    (*(volatile uint8_t *)((0x15) + 0x20)) &= 0b11110111;
    (*(volatile uint8_t *)((0x15) + 0x20)) |= ( 1 << 4 );
    (*(volatile uint8_t *)((0x15) + 0x20)) |= ( 1 << 5 );
    (*(volatile uint8_t *)((0x12) + 0x20)) |= ( 1 << 0 );
    (*(volatile uint8_t *)((0x12) + 0x20)) |= ( 1 << 1 );
    (*(volatile uint8_t *)((0x12) + 0x20)) |= ( 1 << 2 );
    (*(volatile uint8_t *)((0x12) + 0x20)) &= 0b11110111;
  }
  if (a_char == 7) {
    (*(volatile uint8_t *)((0x15) + 0x20)) |= ( 1 << 2 );
    (*(volatile uint8_t *)((0x15) + 0x20)) |= ( 1 << 3 );
    (*(volatile uint8_t *)((0x15) + 0x20)) |= ( 1 << 4 );
    (*(volatile uint8_t *)((0x15) + 0x20)) &= 0b11011111;
    (*(volatile uint8_t *)((0x12) + 0x20)) &= 0b11111110;
    (*(volatile uint8_t *)((0x12) + 0x20)) &= 0b11111101;
    (*(volatile uint8_t *)((0x12) + 0x20)) &= 0b11111011;
    (*(volatile uint8_t *)((0x12) + 0x20)) &= 0b11110111;
  }
  if (a_char == 8) {
    (*(volatile uint8_t *)((0x15) + 0x20)) |= ( 1 << 2 );
    (*(volatile uint8_t *)((0x15) + 0x20)) |= ( 1 << 3 );
    (*(volatile uint8_t *)((0x15) + 0x20)) |= ( 1 << 4 );
    (*(volatile uint8_t *)((0x15) + 0x20)) |= ( 1 << 5 );
    (*(volatile uint8_t *)((0x12) + 0x20)) |= ( 1 << 0 );
    (*(volatile uint8_t *)((0x12) + 0x20)) |= ( 1 << 1 );
    (*(volatile uint8_t *)((0x12) + 0x20)) |= ( 1 << 2 );
    (*(volatile uint8_t *)((0x12) + 0x20)) &= 0b11110111;
  }
  if (a_char == 9) {
    (*(volatile uint8_t *)((0x15) + 0x20)) |= ( 1 << 2 );
    (*(volatile uint8_t *)((0x15) + 0x20)) |= ( 1 << 3 );
    (*(volatile uint8_t *)((0x15) + 0x20)) |= ( 1 << 4 );
    (*(volatile uint8_t *)((0x15) + 0x20)) |= ( 1 << 5 );
    (*(volatile uint8_t *)((0x12) + 0x20)) &= 0b11111110;
    (*(volatile uint8_t *)((0x12) + 0x20)) |= ( 1 << 1 );
    (*(volatile uint8_t *)((0x12) + 0x20)) |= ( 1 << 2 );
    (*(volatile uint8_t *)((0x12) + 0x20)) &= 0b11110111;
  }
  if (a_char == 'o') {
    (*(volatile uint8_t *)((0x15) + 0x20)) |= ( 1 << 2 );
    (*(volatile uint8_t *)((0x15) + 0x20)) |= ( 1 << 3 );
    (*(volatile uint8_t *)((0x15) + 0x20)) &= 0b11101111;
    (*(volatile uint8_t *)((0x15) + 0x20)) &= 0b11011111;
    (*(volatile uint8_t *)((0x12) + 0x20)) &= 0b11111110;
    (*(volatile uint8_t *)((0x12) + 0x20)) |= ( 1 << 1 );
    (*(volatile uint8_t *)((0x12) + 0x20)) |= ( 1 << 2 );
    (*(volatile uint8_t *)((0x12) + 0x20)) &= 0b11110111;
  }
}


int volatile disp_dig = 1;


void lib_7seg_4dig__display(int display_number);
void lib_7seg_4dig__display(int display_number) {
  disp_dig = display_number;
}



int volatile cur_dig_num = 0;



void lib_7seg_4dig__scan(void);
void lib_7seg_4dig__scan(void) {
  cur_dig_num++;
  if (cur_dig_num == 4) {cur_dig_num = 0;}

  char razr_4 = disp_dig % 10;
  char razr_3 = (disp_dig % 100) / 10;
  char razr_2 = (disp_dig % 1000) / 100;
  char razr_1 = (disp_dig) / 1000;


  if (cur_dig_num == 0) {
    (*(volatile uint8_t *)((0x12) + 0x20)) |= ( 1 << 4 );
    (*(volatile uint8_t *)((0x12) + 0x20)) &= 0b11011111;
    (*(volatile uint8_t *)((0x12) + 0x20)) &= 0b10111111;
    (*(volatile uint8_t *)((0x12) + 0x20)) &= 0b01111111;
    set_char(razr_1);
  }
  if (cur_dig_num == 1) {
    (*(volatile uint8_t *)((0x12) + 0x20)) &= 0b11101111;
    (*(volatile uint8_t *)((0x12) + 0x20)) |= ( 1 << 5 );
    (*(volatile uint8_t *)((0x12) + 0x20)) &= 0b10111111;
    (*(volatile uint8_t *)((0x12) + 0x20)) &= 0b01111111;
    set_char(razr_2);
  }
  if (cur_dig_num == 2) {
    (*(volatile uint8_t *)((0x12) + 0x20)) &= 0b11101111;
    (*(volatile uint8_t *)((0x12) + 0x20)) &= 0b11011111;
    (*(volatile uint8_t *)((0x12) + 0x20)) |= ( 1 << 6 );
    (*(volatile uint8_t *)((0x12) + 0x20)) &= 0b01111111;
    set_char(razr_3);
  }
  if (cur_dig_num == 3) {
    (*(volatile uint8_t *)((0x12) + 0x20)) &= 0b11101111;
    (*(volatile uint8_t *)((0x12) + 0x20)) &= 0b11011111;
    (*(volatile uint8_t *)((0x12) + 0x20)) &= 0b10111111;
    (*(volatile uint8_t *)((0x12) + 0x20)) |= ( 1 << 7 );
    set_char(razr_4);
  }
}
# 7 "main.c" 2

int main(void)
{







  int dn = 0;




  while (1) {
    dn++;
    _delay_ms(20);
    lib_7seg_4dig__scan();
    lib_7seg_4dig__display(dn);
  };
# 111 "main.c"
  return 0;

}
