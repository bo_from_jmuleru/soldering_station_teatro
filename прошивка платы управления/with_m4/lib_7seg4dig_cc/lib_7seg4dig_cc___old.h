/*
  m4_avr Библиотека для управления 7-сегментным индикатором с общим катодом
  катоды подключены через транзисторы на землю, то есть тоже управляются
  высоким уровнем
  
  Сконфигурировать нужно пины подключения и функцию инициализации
*/

define(m4_seg_A, m4_pin25)
define(m4_seg_B, m4_pin26)
define(m4_seg_C, m4_pin27)
define(m4_seg_D, m4_pin28)
define(m4_seg_E, m4_pin02)
define(m4_seg_F, m4_pin03)
define(m4_seg_G, m4_pin04)
define(m4_seg_DP, m4_pin05)
define(m4_seg_d1, m4_pin06)
define(m4_seg_d2, m4_pin11)
define(m4_seg_d3, m4_pin12)
define(m4_seg_d4, m4_pin13)

void init_lib_7seg_4dig_cc(void);

void init_lib_7seg_4dig_cc(void) {
  // инициализируем порты на выход
  m4_set_port_as_output(m4_seg_A);
  m4_set_port_as_output(m4_seg_B);
  m4_set_port_as_output(m4_seg_C);
  m4_set_port_as_output(m4_seg_D);
  m4_set_port_as_output(m4_seg_E);
  m4_set_port_as_output(m4_seg_F);
  m4_set_port_as_output(m4_seg_G);
  m4_set_port_as_output(m4_seg_DP);
  m4_set_port_as_output(m4_seg_d1);
  m4_set_port_as_output(m4_seg_d2);
  m4_set_port_as_output(m4_seg_d3);
  m4_set_port_as_output(m4_seg_d4);
  
  m4_set_port_down(m4_seg_A);
  m4_set_port_down(m4_seg_B);
  m4_set_port_down(m4_seg_C);
  m4_set_port_down(m4_seg_D);
  m4_set_port_down(m4_seg_E);
  m4_set_port_down(m4_seg_F);
  m4_set_port_down(m4_seg_G);
  m4_set_port_down(m4_seg_DP);
  m4_set_port_down(m4_seg_d1);
  m4_set_port_down(m4_seg_d2);
  m4_set_port_down(m4_seg_d3);
  m4_set_port_down(m4_seg_d4);
  /*
  // инициализируем таймер развертки
  TCCR0 = (0<<CS02)|(0<<CS01)|(0<<CS00); // делитель
  TIMSK |= (1<<TOIE0); // прерываение по переполнению
  TCNT0 = 0; // начальное значение таймера       
  sei(); // глобальное разрешение прерываний
  */
}

////////////////////////////////////////////////////////////////////////////

// показывает номер текущей цифры в развертке
extern int volatile lib_7seg_4dig__cur_dig_num = 0;

// число, которое на дисплее
extern int volatile lib_7seg_4dig__display = 32;

ISR( TIMER0_OVF_vect )
{
  TCNT0 = 0; 
  
  lib_7seg_4dig__cur_dig_num++;
  if (lib_7seg_4dig__cur_dig_num == 4) {lib_7seg_4dig__cur_dig_num = 0;}
  
  /*
  char razr_4 = lib_7seg_4dig__display % 10;
  char razr_3 = (lib_7seg_4dig__display % 100) / 10;
  char razr_2 = (lib_7seg_4dig__display % 1000) / 100;
  char razr_1 = (lib_7seg_4dig__display) / 1000;
  
  if (lib_7seg_4dig__cur_dig_num == 0) {
    lib_7seg_4dig__set_char(razr_1);
  }
  if (lib_7seg_4dig__cur_dig_num == 1) {
    lib_7seg_4dig__set_char(razr_2);
  }
  if (lib_7seg_4dig__cur_dig_num == 2) {
    lib_7seg_4dig__set_char(razr_3);
  }
  if (lib_7seg_4dig__cur_dig_num == 3) {
    lib_7seg_4dig__set_char(razr_4);
  }
  */
  
  // выставим пин нужной цифры
  if (lib_7seg_4dig__cur_dig_num == 0) {
    m4_set_port_up(m4_seg_d1);
    m4_set_port_down(m4_seg_d2);
    m4_set_port_down(m4_seg_d3);
    m4_set_port_down(m4_seg_d4);
  }
  if (lib_7seg_4dig__cur_dig_num == 1) {
    m4_set_port_down(m4_seg_d1);
    m4_set_port_up(m4_seg_d2);
    m4_set_port_down(m4_seg_d3);
    m4_set_port_down(m4_seg_d4);
  }
  if (lib_7seg_4dig__cur_dig_num == 2) {
    m4_set_port_down(m4_seg_d1);
    m4_set_port_down(m4_seg_d2);
    m4_set_port_up(m4_seg_d3);
    m4_set_port_down(m4_seg_d4);
  }
  if (lib_7seg_4dig__cur_dig_num == 3) {
    m4_set_port_down(m4_seg_d1);
    m4_set_port_down(m4_seg_d2);
    m4_set_port_down(m4_seg_d3);
    m4_set_port_up(m4_seg_d4);
  }
}

void lib_7seg_4dig__set_char(char a_char);
void lib_7seg_4dig__dot_enable();
void lib_7seg_4dig__set_digit();

void lib_7seg_4dig__set_char(char a_char) {
  if (a_char == 0) {
    m4_set_port_up(m4_seg_A);
    m4_set_port_up(m4_seg_B);
    m4_set_port_up(m4_seg_C);
    m4_set_port_up(m4_seg_D);
    m4_set_port_up(m4_seg_E);
    m4_set_port_up(m4_seg_F);
    m4_set_port_down(m4_seg_G);
    m4_set_port_down(m4_seg_DP);
  }
  if (a_char == 1) {
    m4_set_port_down(m4_seg_A);
    m4_set_port_up(m4_seg_B);
    m4_set_port_up(m4_seg_C);
    m4_set_port_down(m4_seg_D);
    m4_set_port_down(m4_seg_E);
    m4_set_port_down(m4_seg_F);
    m4_set_port_down(m4_seg_G);
    m4_set_port_down(m4_seg_DP);
  }
  if (a_char == 2) {
    m4_set_port_up(m4_seg_A);
    m4_set_port_up(m4_seg_B);
    m4_set_port_down(m4_seg_C);
    m4_set_port_up(m4_seg_D);
    m4_set_port_up(m4_seg_E);
    m4_set_port_down(m4_seg_F);
    m4_set_port_up(m4_seg_G);
    m4_set_port_down(m4_seg_DP);
  }
  if (a_char == 3) {
    m4_set_port_up(m4_seg_A);
    m4_set_port_up(m4_seg_B);
    m4_set_port_up(m4_seg_C);
    m4_set_port_up(m4_seg_D);
    m4_set_port_down(m4_seg_E);
    m4_set_port_down(m4_seg_F);
    m4_set_port_up(m4_seg_G);
    m4_set_port_down(m4_seg_DP);
  }
  if (a_char == 4) {
    m4_set_port_down(m4_seg_A);
    m4_set_port_up(m4_seg_B);
    m4_set_port_up(m4_seg_C);
    m4_set_port_down(m4_seg_D);
    m4_set_port_down(m4_seg_E);
    m4_set_port_up(m4_seg_F);
    m4_set_port_up(m4_seg_G);
    m4_set_port_down(m4_seg_DP);
  }
  if (a_char == 5) {
    m4_set_port_up(m4_seg_A);
    m4_set_port_down(m4_seg_B);
    m4_set_port_up(m4_seg_C);
    m4_set_port_up(m4_seg_D);
    m4_set_port_down(m4_seg_E);
    m4_set_port_up(m4_seg_F);
    m4_set_port_up(m4_seg_G);
    m4_set_port_down(m4_seg_DP);
  }
  if (a_char == 6) {
    m4_set_port_up(m4_seg_A);
    m4_set_port_down(m4_seg_B);
    m4_set_port_up(m4_seg_C);
    m4_set_port_up(m4_seg_D);
    m4_set_port_up(m4_seg_E);
    m4_set_port_up(m4_seg_F);
    m4_set_port_up(m4_seg_G);
    m4_set_port_down(m4_seg_DP);
  }
  if (a_char == 7) {
    m4_set_port_up(m4_seg_A);
    m4_set_port_up(m4_seg_B);
    m4_set_port_up(m4_seg_C);
    m4_set_port_down(m4_seg_D);
    m4_set_port_down(m4_seg_E);
    m4_set_port_down(m4_seg_F);
    m4_set_port_down(m4_seg_G);
    m4_set_port_down(m4_seg_DP);
  }
  if (a_char == 8) {
    m4_set_port_up(m4_seg_A);
    m4_set_port_up(m4_seg_B);
    m4_set_port_up(m4_seg_C);
    m4_set_port_up(m4_seg_D);
    m4_set_port_up(m4_seg_E);
    m4_set_port_up(m4_seg_F);
    m4_set_port_up(m4_seg_G);
    m4_set_port_down(m4_seg_DP);
  }
  if (a_char == 9) {
    m4_set_port_up(m4_seg_A);
    m4_set_port_up(m4_seg_B);
    m4_set_port_up(m4_seg_C);
    m4_set_port_up(m4_seg_D);
    m4_set_port_down(m4_seg_E);
    m4_set_port_up(m4_seg_F);
    m4_set_port_up(m4_seg_G);
    m4_set_port_down(m4_seg_DP);
  }
  if (a_char == 'o') {
    m4_set_port_up(m4_seg_A);
    m4_set_port_up(m4_seg_B);
    m4_set_port_down(m4_seg_C);
    m4_set_port_down(m4_seg_D);
    m4_set_port_down(m4_seg_E);
    m4_set_port_up(m4_seg_F);
    m4_set_port_up(m4_seg_G);
    m4_set_port_down(m4_seg_DP);
  }
}

void lib_7seg_4dig__dot_enable() {
  m4_set_port_up(m4_seg_DP);
}
