# 1 "main.c"
# 1 "/home/max/kicad/soldering_station_teatro/soldering_station_teatro/прошивка платы управления/with_m4/lib_7seg4dig_cc//"
# 1 "<command-line>"
# 1 "main.c"
# 1 "/usr/lib/avr/include/avr/io.h" 1 3
# 99 "/usr/lib/avr/include/avr/io.h" 3
# 1 "/usr/lib/avr/include/avr/sfr_defs.h" 1 3
# 126 "/usr/lib/avr/include/avr/sfr_defs.h" 3
# 1 "/usr/lib/avr/include/inttypes.h" 1 3
# 37 "/usr/lib/avr/include/inttypes.h" 3
# 1 "/usr/lib/gcc/avr/4.8.1/include/stdint.h" 1 3 4
# 9 "/usr/lib/gcc/avr/4.8.1/include/stdint.h" 3 4
# 1 "/usr/lib/avr/include/stdint.h" 1 3 4
# 121 "/usr/lib/avr/include/stdint.h" 3 4
typedef signed int int8_t __attribute__((__mode__(__QI__)));
typedef unsigned int uint8_t __attribute__((__mode__(__QI__)));
typedef signed int int16_t __attribute__ ((__mode__ (__HI__)));
typedef unsigned int uint16_t __attribute__ ((__mode__ (__HI__)));
typedef signed int int32_t __attribute__ ((__mode__ (__SI__)));
typedef unsigned int uint32_t __attribute__ ((__mode__ (__SI__)));

typedef signed int int64_t __attribute__((__mode__(__DI__)));
typedef unsigned int uint64_t __attribute__((__mode__(__DI__)));
# 142 "/usr/lib/avr/include/stdint.h" 3 4
typedef int16_t intptr_t;




typedef uint16_t uintptr_t;
# 159 "/usr/lib/avr/include/stdint.h" 3 4
typedef int8_t int_least8_t;




typedef uint8_t uint_least8_t;




typedef int16_t int_least16_t;




typedef uint16_t uint_least16_t;




typedef int32_t int_least32_t;




typedef uint32_t uint_least32_t;







typedef int64_t int_least64_t;






typedef uint64_t uint_least64_t;
# 213 "/usr/lib/avr/include/stdint.h" 3 4
typedef int8_t int_fast8_t;




typedef uint8_t uint_fast8_t;




typedef int16_t int_fast16_t;




typedef uint16_t uint_fast16_t;




typedef int32_t int_fast32_t;




typedef uint32_t uint_fast32_t;







typedef int64_t int_fast64_t;






typedef uint64_t uint_fast64_t;
# 273 "/usr/lib/avr/include/stdint.h" 3 4
typedef int64_t intmax_t;




typedef uint64_t uintmax_t;
# 10 "/usr/lib/gcc/avr/4.8.1/include/stdint.h" 2 3 4
# 38 "/usr/lib/avr/include/inttypes.h" 2 3
# 77 "/usr/lib/avr/include/inttypes.h" 3
typedef int32_t int_farptr_t;



typedef uint32_t uint_farptr_t;
# 127 "/usr/lib/avr/include/avr/sfr_defs.h" 2 3
# 100 "/usr/lib/avr/include/avr/io.h" 2 3
# 348 "/usr/lib/avr/include/avr/io.h" 3
# 1 "/usr/lib/avr/include/avr/iom8.h" 1 3
# 636 "/usr/lib/avr/include/avr/iom8.h" 3
       
# 637 "/usr/lib/avr/include/avr/iom8.h" 3

       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
# 349 "/usr/lib/avr/include/avr/io.h" 2 3
# 610 "/usr/lib/avr/include/avr/io.h" 3
# 1 "/usr/lib/avr/include/avr/portpins.h" 1 3
# 611 "/usr/lib/avr/include/avr/io.h" 2 3

# 1 "/usr/lib/avr/include/avr/common.h" 1 3
# 613 "/usr/lib/avr/include/avr/io.h" 2 3

# 1 "/usr/lib/avr/include/avr/version.h" 1 3
# 615 "/usr/lib/avr/include/avr/io.h" 2 3






# 1 "/usr/lib/avr/include/avr/fuse.h" 1 3
# 248 "/usr/lib/avr/include/avr/fuse.h" 3
typedef struct
{
    unsigned char low;
    unsigned char high;
} __fuse_t;
# 622 "/usr/lib/avr/include/avr/io.h" 2 3


# 1 "/usr/lib/avr/include/avr/lock.h" 1 3
# 625 "/usr/lib/avr/include/avr/io.h" 2 3
# 2 "main.c" 2

# 1 "/usr/lib/avr/include/util/delay.h" 1 3
# 43 "/usr/lib/avr/include/util/delay.h" 3
# 1 "/usr/lib/avr/include/util/delay_basic.h" 1 3
# 40 "/usr/lib/avr/include/util/delay_basic.h" 3
static inline void _delay_loop_1(uint8_t __count) __attribute__((always_inline));
static inline void _delay_loop_2(uint16_t __count) __attribute__((always_inline));
# 80 "/usr/lib/avr/include/util/delay_basic.h" 3
void
_delay_loop_1(uint8_t __count)
{
 __asm__ volatile (
  "1: dec %0" "\n\t"
  "brne 1b"
  : "=r" (__count)
  : "0" (__count)
 );
}
# 102 "/usr/lib/avr/include/util/delay_basic.h" 3
void
_delay_loop_2(uint16_t __count)
{
 __asm__ volatile (
  "1: sbiw %0,1" "\n\t"
  "brne 1b"
  : "=w" (__count)
  : "0" (__count)
 );
}
# 44 "/usr/lib/avr/include/util/delay.h" 2 3
# 1 "/usr/lib/avr/include/math.h" 1 3
# 127 "/usr/lib/avr/include/math.h" 3
extern double cos(double __x) __attribute__((__const__));





extern double sin(double __x) __attribute__((__const__));





extern double tan(double __x) __attribute__((__const__));






extern double fabs(double __x) __attribute__((__const__));






extern double fmod(double __x, double __y) __attribute__((__const__));
# 168 "/usr/lib/avr/include/math.h" 3
extern double modf(double __x, double *__iptr);



extern float modff (float __x, float *__iptr);




extern double sqrt(double __x) __attribute__((__const__));





extern double cbrt(double __x) __attribute__((__const__));
# 194 "/usr/lib/avr/include/math.h" 3
extern double hypot (double __x, double __y) __attribute__((__const__));







extern double square(double __x) __attribute__((__const__));






extern double floor(double __x) __attribute__((__const__));






extern double ceil(double __x) __attribute__((__const__));
# 234 "/usr/lib/avr/include/math.h" 3
extern double frexp(double __x, int *__pexp);







extern double ldexp(double __x, int __exp) __attribute__((__const__));





extern double exp(double __x) __attribute__((__const__));





extern double cosh(double __x) __attribute__((__const__));





extern double sinh(double __x) __attribute__((__const__));





extern double tanh(double __x) __attribute__((__const__));







extern double acos(double __x) __attribute__((__const__));







extern double asin(double __x) __attribute__((__const__));






extern double atan(double __x) __attribute__((__const__));
# 298 "/usr/lib/avr/include/math.h" 3
extern double atan2(double __y, double __x) __attribute__((__const__));





extern double log(double __x) __attribute__((__const__));





extern double log10(double __x) __attribute__((__const__));





extern double pow(double __x, double __y) __attribute__((__const__));






extern int isnan(double __x) __attribute__((__const__));
# 333 "/usr/lib/avr/include/math.h" 3
extern int isinf(double __x) __attribute__((__const__));






__attribute__((__const__)) static inline int isfinite (double __x)
{
    unsigned char __exp;
    __asm__ (
 "mov	%0, %C1		\n\t"
 "lsl	%0		\n\t"
 "mov	%0, %D1		\n\t"
 "rol	%0		"
 : "=r" (__exp)
 : "r" (__x) );
    return __exp != 0xff;
}






__attribute__((__const__)) static inline double copysign (double __x, double __y)
{
    __asm__ (
 "bst	%D2, 7	\n\t"
 "bld	%D0, 7	"
 : "=r" (__x)
 : "0" (__x), "r" (__y) );
    return __x;
}
# 376 "/usr/lib/avr/include/math.h" 3
extern int signbit (double __x) __attribute__((__const__));






extern double fdim (double __x, double __y) __attribute__((__const__));
# 392 "/usr/lib/avr/include/math.h" 3
extern double fma (double __x, double __y, double __z) __attribute__((__const__));







extern double fmax (double __x, double __y) __attribute__((__const__));







extern double fmin (double __x, double __y) __attribute__((__const__));






extern double trunc (double __x) __attribute__((__const__));
# 426 "/usr/lib/avr/include/math.h" 3
extern double round (double __x) __attribute__((__const__));
# 439 "/usr/lib/avr/include/math.h" 3
extern long lround (double __x) __attribute__((__const__));
# 453 "/usr/lib/avr/include/math.h" 3
extern long lrint (double __x) __attribute__((__const__));
# 45 "/usr/lib/avr/include/util/delay.h" 2 3
# 84 "/usr/lib/avr/include/util/delay.h" 3
static inline void _delay_us(double __us) __attribute__((always_inline));
static inline void _delay_ms(double __ms) __attribute__((always_inline));
# 141 "/usr/lib/avr/include/util/delay.h" 3
void
_delay_ms(double __ms)
{
 double __tmp ;



 uint32_t __ticks_dc;
 extern void __builtin_avr_delay_cycles(unsigned long);
 __tmp = ((4000000UL) / 1e3) * __ms;
# 160 "/usr/lib/avr/include/util/delay.h" 3
  __ticks_dc = (uint32_t)(ceil(fabs(__tmp)));


 __builtin_avr_delay_cycles(__ticks_dc);
# 186 "/usr/lib/avr/include/util/delay.h" 3
}
# 223 "/usr/lib/avr/include/util/delay.h" 3
void
_delay_us(double __us)
{
 double __tmp ;



 uint32_t __ticks_dc;
 extern void __builtin_avr_delay_cycles(unsigned long);
 __tmp = ((4000000UL) / 1e6) * __us;
# 242 "/usr/lib/avr/include/util/delay.h" 3
  __ticks_dc = (uint32_t)(ceil(fabs(__tmp)));


 __builtin_avr_delay_cycles(__ticks_dc);
# 268 "/usr/lib/avr/include/util/delay.h" 3
}
# 4 "main.c" 2
# 1 "m4_avr.h" 1
changequote([,])



define(m4_set_port_as_output, m4_set_$1_as_output)
define(m4_set_port_as_input, m4_set_$1_as_input)
define(m4_set_port_up, m4_set_$1_up)
define(m4_set_port_down, m4_set_$1_down)
define(m4_get_port_state, m4_get_$1_state)


define(m4_set_pb0_as_output, (*(volatile uint8_t *)((0x17) + 0x20)) |= ( 1 << 0 ))
define(m4_set_pb1_as_output, (*(volatile uint8_t *)((0x17) + 0x20)) |= ( 1 << 1 ))
define(m4_set_pb2_as_output, (*(volatile uint8_t *)((0x17) + 0x20)) |= ( 1 << 2 ))
define(m4_set_pb3_as_output, (*(volatile uint8_t *)((0x17) + 0x20)) |= ( 1 << 3 ))
define(m4_set_pb4_as_output, (*(volatile uint8_t *)((0x17) + 0x20)) |= ( 1 << 4 ))
define(m4_set_pb5_as_output, (*(volatile uint8_t *)((0x17) + 0x20)) |= ( 1 << 5 ))
define(m4_set_pb6_as_output, (*(volatile uint8_t *)((0x17) + 0x20)) |= ( 1 << 6 ))
define(m4_set_pb7_as_output, (*(volatile uint8_t *)((0x17) + 0x20)) |= ( 1 << 7 ))
define(m4_set_pc0_as_output, (*(volatile uint8_t *)((0x14) + 0x20)) |= ( 1 << 0 ))
define(m4_set_pc1_as_output, (*(volatile uint8_t *)((0x14) + 0x20)) |= ( 1 << 1 ))
define(m4_set_pc2_as_output, (*(volatile uint8_t *)((0x14) + 0x20)) |= ( 1 << 2 ))
define(m4_set_pc3_as_output, (*(volatile uint8_t *)((0x14) + 0x20)) |= ( 1 << 3 ))
define(m4_set_pc4_as_output, (*(volatile uint8_t *)((0x14) + 0x20)) |= ( 1 << 4 ))
define(m4_set_pc5_as_output, (*(volatile uint8_t *)((0x14) + 0x20)) |= ( 1 << 5 ))
define(m4_set_pc6_as_output, (*(volatile uint8_t *)((0x14) + 0x20)) |= ( 1 << 6 ))
define(m4_set_pc7_as_output, (*(volatile uint8_t *)((0x14) + 0x20)) |= ( 1 << 7 ))
define(m4_set_pd0_as_output, (*(volatile uint8_t *)((0x11) + 0x20)) |= ( 1 << 0 ))
define(m4_set_pd1_as_output, (*(volatile uint8_t *)((0x11) + 0x20)) |= ( 1 << 1 ))
define(m4_set_pd2_as_output, (*(volatile uint8_t *)((0x11) + 0x20)) |= ( 1 << 2 ))
define(m4_set_pd3_as_output, (*(volatile uint8_t *)((0x11) + 0x20)) |= ( 1 << 3 ))
define(m4_set_pd4_as_output, (*(volatile uint8_t *)((0x11) + 0x20)) |= ( 1 << 4 ))
define(m4_set_pd5_as_output, (*(volatile uint8_t *)((0x11) + 0x20)) |= ( 1 << 5 ))
define(m4_set_pd6_as_output, (*(volatile uint8_t *)((0x11) + 0x20)) |= ( 1 << 6 ))
define(m4_set_pd7_as_output, (*(volatile uint8_t *)((0x11) + 0x20)) |= ( 1 << 7 ))
define(m4_set_pb0_as_input, (*(volatile uint8_t *)((0x17) + 0x20)) &= 0b11111110)
define(m4_set_pb1_as_input, (*(volatile uint8_t *)((0x17) + 0x20)) &= 0b11111101)
define(m4_set_pb2_as_input, (*(volatile uint8_t *)((0x17) + 0x20)) &= 0b11111011)
define(m4_set_pb3_as_input, (*(volatile uint8_t *)((0x17) + 0x20)) &= 0b11110111)
define(m4_set_pb4_as_input, (*(volatile uint8_t *)((0x17) + 0x20)) &= 0b11101111)
define(m4_set_pb5_as_input, (*(volatile uint8_t *)((0x17) + 0x20)) &= 0b11011111)
define(m4_set_pb6_as_input, (*(volatile uint8_t *)((0x17) + 0x20)) &= 0b10111111)
define(m4_set_pb7_as_input, (*(volatile uint8_t *)((0x17) + 0x20)) &= 0b01111111)
define(m4_set_pc0_as_input, (*(volatile uint8_t *)((0x14) + 0x20)) &= 0b11111110)
define(m4_set_pc1_as_input, (*(volatile uint8_t *)((0x14) + 0x20)) &= 0b11111101)
define(m4_set_pc2_as_input, (*(volatile uint8_t *)((0x14) + 0x20)) &= 0b11111011)
define(m4_set_pc3_as_input, (*(volatile uint8_t *)((0x14) + 0x20)) &= 0b11110111)
define(m4_set_pc4_as_input, (*(volatile uint8_t *)((0x14) + 0x20)) &= 0b11101111)
define(m4_set_pc5_as_input, (*(volatile uint8_t *)((0x14) + 0x20)) &= 0b11011111)
define(m4_set_pc6_as_input, (*(volatile uint8_t *)((0x14) + 0x20)) &= 0b10111111)
define(m4_set_pc7_as_input, (*(volatile uint8_t *)((0x14) + 0x20)) &= 0b01111111)
define(m4_set_pd0_as_input, (*(volatile uint8_t *)((0x11) + 0x20)) &= 0b11111110)
define(m4_set_pd1_as_input, (*(volatile uint8_t *)((0x11) + 0x20)) &= 0b11111101)
define(m4_set_pd2_as_input, (*(volatile uint8_t *)((0x11) + 0x20)) &= 0b11111011)
define(m4_set_pd3_as_input, (*(volatile uint8_t *)((0x11) + 0x20)) &= 0b11110111)
define(m4_set_pd4_as_input, (*(volatile uint8_t *)((0x11) + 0x20)) &= 0b11101111)
define(m4_set_pd5_as_input, (*(volatile uint8_t *)((0x11) + 0x20)) &= 0b11011111)
define(m4_set_pd6_as_input, (*(volatile uint8_t *)((0x11) + 0x20)) &= 0b10111111)
define(m4_set_pd7_as_input, (*(volatile uint8_t *)((0x11) + 0x20)) &= 0b01111111)


define(m4_set_pb0_up, (*(volatile uint8_t *)((0x18) + 0x20)) |= ( 1 << 0 ))
define(m4_set_pb1_up, (*(volatile uint8_t *)((0x18) + 0x20)) |= ( 1 << 1 ))
define(m4_set_pb2_up, (*(volatile uint8_t *)((0x18) + 0x20)) |= ( 1 << 2 ))
define(m4_set_pb3_up, (*(volatile uint8_t *)((0x18) + 0x20)) |= ( 1 << 3 ))
define(m4_set_pb4_up, (*(volatile uint8_t *)((0x18) + 0x20)) |= ( 1 << 4 ))
define(m4_set_pb5_up, (*(volatile uint8_t *)((0x18) + 0x20)) |= ( 1 << 5 ))
define(m4_set_pb6_up, (*(volatile uint8_t *)((0x18) + 0x20)) |= ( 1 << 6 ))
define(m4_set_pb7_up, (*(volatile uint8_t *)((0x18) + 0x20)) |= ( 1 << 7 ))
define(m4_set_pc0_up, (*(volatile uint8_t *)((0x15) + 0x20)) |= ( 1 << 0 ))
define(m4_set_pc1_up, (*(volatile uint8_t *)((0x15) + 0x20)) |= ( 1 << 1 ))
define(m4_set_pc2_up, (*(volatile uint8_t *)((0x15) + 0x20)) |= ( 1 << 2 ))
define(m4_set_pc3_up, (*(volatile uint8_t *)((0x15) + 0x20)) |= ( 1 << 3 ))
define(m4_set_pc4_up, (*(volatile uint8_t *)((0x15) + 0x20)) |= ( 1 << 4 ))
define(m4_set_pc5_up, (*(volatile uint8_t *)((0x15) + 0x20)) |= ( 1 << 5 ))
define(m4_set_pc6_up, (*(volatile uint8_t *)((0x15) + 0x20)) |= ( 1 << 6 ))
define(m4_set_pc7_up, (*(volatile uint8_t *)((0x15) + 0x20)) |= ( 1 << 7 ))
define(m4_set_pd0_up, (*(volatile uint8_t *)((0x12) + 0x20)) |= ( 1 << 0 ))
define(m4_set_pd1_up, (*(volatile uint8_t *)((0x12) + 0x20)) |= ( 1 << 1 ))
define(m4_set_pd2_up, (*(volatile uint8_t *)((0x12) + 0x20)) |= ( 1 << 2 ))
define(m4_set_pd3_up, (*(volatile uint8_t *)((0x12) + 0x20)) |= ( 1 << 3 ))
define(m4_set_pd4_up, (*(volatile uint8_t *)((0x12) + 0x20)) |= ( 1 << 4 ))
define(m4_set_pd5_up, (*(volatile uint8_t *)((0x12) + 0x20)) |= ( 1 << 5 ))
define(m4_set_pd6_up, (*(volatile uint8_t *)((0x12) + 0x20)) |= ( 1 << 6 ))
define(m4_set_pd7_up, (*(volatile uint8_t *)((0x12) + 0x20)) |= ( 1 << 7 ))
define(m4_set_pb0_down, (*(volatile uint8_t *)((0x18) + 0x20)) &= 0b11111110)
define(m4_set_pb1_down, (*(volatile uint8_t *)((0x18) + 0x20)) &= 0b11111101)
define(m4_set_pb2_down, (*(volatile uint8_t *)((0x18) + 0x20)) &= 0b11111011)
define(m4_set_pb3_down, (*(volatile uint8_t *)((0x18) + 0x20)) &= 0b11110111)
define(m4_set_pb4_down, (*(volatile uint8_t *)((0x18) + 0x20)) &= 0b11101111)
define(m4_set_pb5_down, (*(volatile uint8_t *)((0x18) + 0x20)) &= 0b11011111)
define(m4_set_pb6_down, (*(volatile uint8_t *)((0x18) + 0x20)) &= 0b10111111)
define(m4_set_pb7_down, (*(volatile uint8_t *)((0x18) + 0x20)) &= 0b01111111)
define(m4_set_pc0_down, (*(volatile uint8_t *)((0x15) + 0x20)) &= 0b11111110)
define(m4_set_pc1_down, (*(volatile uint8_t *)((0x15) + 0x20)) &= 0b11111101)
define(m4_set_pc2_down, (*(volatile uint8_t *)((0x15) + 0x20)) &= 0b11111011)
define(m4_set_pc3_down, (*(volatile uint8_t *)((0x15) + 0x20)) &= 0b11110111)
define(m4_set_pc4_down, (*(volatile uint8_t *)((0x15) + 0x20)) &= 0b11101111)
define(m4_set_pc5_down, (*(volatile uint8_t *)((0x15) + 0x20)) &= 0b11011111)
define(m4_set_pc6_down, (*(volatile uint8_t *)((0x15) + 0x20)) &= 0b10111111)
define(m4_set_pc7_down, (*(volatile uint8_t *)((0x15) + 0x20)) &= 0b01111111)
define(m4_set_pd0_down, (*(volatile uint8_t *)((0x12) + 0x20)) &= 0b11111110)
define(m4_set_pd1_down, (*(volatile uint8_t *)((0x12) + 0x20)) &= 0b11111101)
define(m4_set_pd2_down, (*(volatile uint8_t *)((0x12) + 0x20)) &= 0b11111011)
define(m4_set_pd3_down, (*(volatile uint8_t *)((0x12) + 0x20)) &= 0b11110111)
define(m4_set_pd4_down, (*(volatile uint8_t *)((0x12) + 0x20)) &= 0b11101111)
define(m4_set_pd5_down, (*(volatile uint8_t *)((0x12) + 0x20)) &= 0b11011111)
define(m4_set_pd6_down, (*(volatile uint8_t *)((0x12) + 0x20)) &= 0b10111111)
define(m4_set_pd7_down, (*(volatile uint8_t *)((0x12) + 0x20)) &= 0b01111111)


define(m4_get_pb0_state, (1 << 0) & (*(volatile uint8_t *)((0x18) + 0x20)))
define(m4_get_pb1_state, (1 << 1) & (*(volatile uint8_t *)((0x18) + 0x20)))
define(m4_get_pb2_state, (1 << 2) & (*(volatile uint8_t *)((0x18) + 0x20)))
define(m4_get_pb3_state, (1 << 3) & (*(volatile uint8_t *)((0x18) + 0x20)))
define(m4_get_pb4_state, (1 << 4) & (*(volatile uint8_t *)((0x18) + 0x20)))
define(m4_get_pb5_state, (1 << 5) & (*(volatile uint8_t *)((0x18) + 0x20)))
define(m4_get_pb6_state, (1 << 6) & (*(volatile uint8_t *)((0x18) + 0x20)))
define(m4_get_pb7_state, (1 << 7) & (*(volatile uint8_t *)((0x18) + 0x20)))
define(m4_get_pc0_state, (1 << 0) & (*(volatile uint8_t *)((0x15) + 0x20)))
define(m4_get_pc1_state, (1 << 1) & (*(volatile uint8_t *)((0x15) + 0x20)))
define(m4_get_pc2_state, (1 << 2) & (*(volatile uint8_t *)((0x15) + 0x20)))
define(m4_get_pc3_state, (1 << 3) & (*(volatile uint8_t *)((0x15) + 0x20)))
define(m4_get_pc4_state, (1 << 4) & (*(volatile uint8_t *)((0x15) + 0x20)))
define(m4_get_pc5_state, (1 << 5) & (*(volatile uint8_t *)((0x15) + 0x20)))
define(m4_get_pc6_state, (1 << 6) & (*(volatile uint8_t *)((0x15) + 0x20)))
define(m4_get_pc7_state, (1 << 7) & (*(volatile uint8_t *)((0x15) + 0x20)))
define(m4_get_pd0_state, (1 << 0) & (*(volatile uint8_t *)((0x12) + 0x20)))
define(m4_get_pd1_state, (1 << 1) & (*(volatile uint8_t *)((0x12) + 0x20)))
define(m4_get_pd2_state, (1 << 2) & (*(volatile uint8_t *)((0x12) + 0x20)))
define(m4_get_pd3_state, (1 << 3) & (*(volatile uint8_t *)((0x12) + 0x20)))
define(m4_get_pd4_state, (1 << 4) & (*(volatile uint8_t *)((0x12) + 0x20)))
define(m4_get_pd5_state, (1 << 5) & (*(volatile uint8_t *)((0x12) + 0x20)))
define(m4_get_pd6_state, (1 << 6) & (*(volatile uint8_t *)((0x12) + 0x20)))
define(m4_get_pd7_state, (1 << 7) & (*(volatile uint8_t *)((0x12) + 0x20)))
# 5 "main.c" 2
# 1 "m4_atmega8_dip28.h" 1

define(m4_pin01, pc6)
define(m4_pin02, pd0)
define(m4_pin03, pd1)
define(m4_pin04, pd2)
define(m4_pin05, pd3)
define(m4_pin06, pd4)
define(m4_pin09, pb6)
define(m4_pin10, pb7)
define(m4_pin11, pd5)
define(m4_pin12, pd6)
define(m4_pin13, pd7)
define(m4_pin14, pb0)
define(m4_pin15, pb1)
define(m4_pin16, pb2)
define(m4_pin17, pb3)
define(m4_pin18, pb4)
define(m4_pin19, pb5)
define(m4_pin23, pc0)
define(m4_pin24, pc1)
define(m4_pin25, pc2)
define(m4_pin26, pc3)
define(m4_pin27, pc4)
define(m4_pin28, pc5)
# 6 "main.c" 2
# 1 "lib_7seg4dig_cc.h" 1
# 13 "lib_7seg4dig_cc.h"
define(lib_7seg_4dig_cc__init,
  [
  define(m4_seg_A, $1)
  define(m4_seg_B, $2)
  define(m4_seg_C, $3)
  define(m4_seg_D, $4)
  define(m4_seg_E, $5)
  define(m4_seg_F, $6)
  define(m4_seg_G, $7)
  define(m4_seg_DP, $8)
  define(m4_seg_d1, $9)
  define(m4_seg_d2, $10)
  define(m4_seg_d3, $11)
  define(m4_seg_d4, $12)
  ]
)

  lib_7seg_4dig_cc__init(
    m4_pin25,
    m4_pin26,
    m4_pin27,
    m4_pin28,
    m4_pin02,
    m4_pin03,
    m4_pin04,
    m4_pin05,
    m4_pin06,
    m4_pin11,
    m4_pin12,
    m4_pin13);

void func(void) {

  m4_set_port_as_output(m4_seg_A);
  m4_set_port_as_output(m4_seg_B);
  m4_set_port_as_output(m4_seg_C);
  m4_set_port_as_output(m4_seg_D);
  m4_set_port_as_output(m4_seg_E);
  m4_set_port_as_output(m4_seg_F);
  m4_set_port_as_output(m4_seg_G);
  m4_set_port_as_output(m4_seg_DP);
  m4_set_port_as_output(m4_seg_d1);
  m4_set_port_as_output(m4_seg_d2);
  m4_set_port_as_output(m4_seg_d3);
  m4_set_port_as_output(m4_seg_d4);

  m4_set_port_down(m4_seg_A);
  m4_set_port_down(m4_seg_B);
  m4_set_port_down(m4_seg_C);
  m4_set_port_down(m4_seg_D);
  m4_set_port_down(m4_seg_E);
  m4_set_port_down(m4_seg_F);
  m4_set_port_down(m4_seg_G);
  m4_set_port_down(m4_seg_DP);
  m4_set_port_down(m4_seg_d1);
  m4_set_port_down(m4_seg_d2);
  m4_set_port_down(m4_seg_d3);
  m4_set_port_down(m4_seg_d4);
}

void set_char(char a_char) {
  if (a_char == 0) {
    m4_set_port_up(m4_seg_A);
    m4_set_port_up(m4_seg_B);
    m4_set_port_up(m4_seg_C);
    m4_set_port_up(m4_seg_D);
    m4_set_port_up(m4_seg_E);
    m4_set_port_up(m4_seg_F);
    m4_set_port_down(m4_seg_G);
    m4_set_port_down(m4_seg_DP);
  }
  if (a_char == 1) {
    m4_set_port_down(m4_seg_A);
    m4_set_port_up(m4_seg_B);
    m4_set_port_up(m4_seg_C);
    m4_set_port_down(m4_seg_D);
    m4_set_port_down(m4_seg_E);
    m4_set_port_down(m4_seg_F);
    m4_set_port_down(m4_seg_G);
    m4_set_port_down(m4_seg_DP);
  }
  if (a_char == 2) {
    m4_set_port_up(m4_seg_A);
    m4_set_port_up(m4_seg_B);
    m4_set_port_down(m4_seg_C);
    m4_set_port_up(m4_seg_D);
    m4_set_port_up(m4_seg_E);
    m4_set_port_down(m4_seg_F);
    m4_set_port_up(m4_seg_G);
    m4_set_port_down(m4_seg_DP);
  }
  if (a_char == 3) {
    m4_set_port_up(m4_seg_A);
    m4_set_port_up(m4_seg_B);
    m4_set_port_up(m4_seg_C);
    m4_set_port_up(m4_seg_D);
    m4_set_port_down(m4_seg_E);
    m4_set_port_down(m4_seg_F);
    m4_set_port_up(m4_seg_G);
    m4_set_port_down(m4_seg_DP);
  }
  if (a_char == 4) {
    m4_set_port_down(m4_seg_A);
    m4_set_port_up(m4_seg_B);
    m4_set_port_up(m4_seg_C);
    m4_set_port_down(m4_seg_D);
    m4_set_port_down(m4_seg_E);
    m4_set_port_up(m4_seg_F);
    m4_set_port_up(m4_seg_G);
    m4_set_port_down(m4_seg_DP);
  }
  if (a_char == 5) {
    m4_set_port_up(m4_seg_A);
    m4_set_port_down(m4_seg_B);
    m4_set_port_up(m4_seg_C);
    m4_set_port_up(m4_seg_D);
    m4_set_port_down(m4_seg_E);
    m4_set_port_up(m4_seg_F);
    m4_set_port_up(m4_seg_G);
    m4_set_port_down(m4_seg_DP);
  }
  if (a_char == 6) {
    m4_set_port_up(m4_seg_A);
    m4_set_port_down(m4_seg_B);
    m4_set_port_up(m4_seg_C);
    m4_set_port_up(m4_seg_D);
    m4_set_port_up(m4_seg_E);
    m4_set_port_up(m4_seg_F);
    m4_set_port_up(m4_seg_G);
    m4_set_port_down(m4_seg_DP);
  }
  if (a_char == 7) {
    m4_set_port_up(m4_seg_A);
    m4_set_port_up(m4_seg_B);
    m4_set_port_up(m4_seg_C);
    m4_set_port_down(m4_seg_D);
    m4_set_port_down(m4_seg_E);
    m4_set_port_down(m4_seg_F);
    m4_set_port_down(m4_seg_G);
    m4_set_port_down(m4_seg_DP);
  }
  if (a_char == 8) {
    m4_set_port_up(m4_seg_A);
    m4_set_port_up(m4_seg_B);
    m4_set_port_up(m4_seg_C);
    m4_set_port_up(m4_seg_D);
    m4_set_port_up(m4_seg_E);
    m4_set_port_up(m4_seg_F);
    m4_set_port_up(m4_seg_G);
    m4_set_port_down(m4_seg_DP);
  }
  if (a_char == 9) {
    m4_set_port_up(m4_seg_A);
    m4_set_port_up(m4_seg_B);
    m4_set_port_up(m4_seg_C);
    m4_set_port_up(m4_seg_D);
    m4_set_port_down(m4_seg_E);
    m4_set_port_up(m4_seg_F);
    m4_set_port_up(m4_seg_G);
    m4_set_port_down(m4_seg_DP);
  }
  if (a_char == 'o') {
    m4_set_port_up(m4_seg_A);
    m4_set_port_up(m4_seg_B);
    m4_set_port_down(m4_seg_C);
    m4_set_port_down(m4_seg_D);
    m4_set_port_down(m4_seg_E);
    m4_set_port_up(m4_seg_F);
    m4_set_port_up(m4_seg_G);
    m4_set_port_down(m4_seg_DP);
  }
}


int volatile disp_dig = 1;


void lib_7seg_4dig__display(int display_number);
void lib_7seg_4dig__display(int display_number) {
  disp_dig = display_number;
}



int volatile cur_dig_num = 0;



void lib_7seg_4dig__scan(void);
void lib_7seg_4dig__scan(void) {
  cur_dig_num++;
  if (cur_dig_num == 4) {cur_dig_num = 0;}

  char razr_4 = disp_dig % 10;
  char razr_3 = (disp_dig % 100) / 10;
  char razr_2 = (disp_dig % 1000) / 100;
  char razr_1 = (disp_dig) / 1000;


  if (cur_dig_num == 0) {
    m4_set_port_up(m4_seg_d1);
    m4_set_port_down(m4_seg_d2);
    m4_set_port_down(m4_seg_d3);
    m4_set_port_down(m4_seg_d4);
    set_char(razr_1);
  }
  if (cur_dig_num == 1) {
    m4_set_port_down(m4_seg_d1);
    m4_set_port_up(m4_seg_d2);
    m4_set_port_down(m4_seg_d3);
    m4_set_port_down(m4_seg_d4);
    set_char(razr_2);
  }
  if (cur_dig_num == 2) {
    m4_set_port_down(m4_seg_d1);
    m4_set_port_down(m4_seg_d2);
    m4_set_port_up(m4_seg_d3);
    m4_set_port_down(m4_seg_d4);
    set_char(razr_3);
  }
  if (cur_dig_num == 3) {
    m4_set_port_down(m4_seg_d1);
    m4_set_port_down(m4_seg_d2);
    m4_set_port_down(m4_seg_d3);
    m4_set_port_up(m4_seg_d4);
    set_char(razr_4);
  }
}
# 7 "main.c" 2

int main(void)
{







  int dn = 0;




  while (1) {
    dn++;
    _delay_ms(20);
    lib_7seg_4dig__scan();
    lib_7seg_4dig__display(dn);
  };
# 111 "main.c"
  return 0;

}
