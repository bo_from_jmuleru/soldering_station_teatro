/*
  m4_avr Библиотека для управления 7-сегментным индикатором с общим катодом
  катоды подключены через транзисторы на землю, то есть тоже управляются
  высоким уровнем
  
  Сконфигурировать нужно пины подключения
*/



////////////////////////////////////////////////////////////////////////////

define(lib_7seg_4dig_cc__init,
  [
  define(m4_seg_A, $1)
  define(m4_seg_B, $2)
  define(m4_seg_C, $3)
  define(m4_seg_D, $4)
  define(m4_seg_E, $5)
  define(m4_seg_F, $6)
  define(m4_seg_G, $7)
  define(m4_seg_DP, $8)
  define(m4_seg_d1, $9)
  define(m4_seg_d2, $10)
  define(m4_seg_d3, $11)
  define(m4_seg_d4, $12)
  ]
)

  lib_7seg_4dig_cc__init(
    m4_pin25, // сегмент А индикатора
    m4_pin26, // сегмент В индикатора
    m4_pin27, // сегмент С индикатора
    m4_pin28, // сегмент D индикатора
    m4_pin02, // сегмент Е индикатора
    m4_pin03, // сегмент F индикатора
    m4_pin04, // сегмент G индикатора
    m4_pin05, // сегмент DP (точка) индикатора
    m4_pin06, // цифра 1
    m4_pin11, // цифра 2
    m4_pin12, // цифра 3
    m4_pin13);  // цифра 4

void func(void) {
  // инициализируем порты на выход
  m4_set_port_as_output(m4_seg_A);
  m4_set_port_as_output(m4_seg_B);
  m4_set_port_as_output(m4_seg_C);
  m4_set_port_as_output(m4_seg_D);
  m4_set_port_as_output(m4_seg_E);
  m4_set_port_as_output(m4_seg_F);
  m4_set_port_as_output(m4_seg_G);
  m4_set_port_as_output(m4_seg_DP);
  m4_set_port_as_output(m4_seg_d1);
  m4_set_port_as_output(m4_seg_d2);
  m4_set_port_as_output(m4_seg_d3);
  m4_set_port_as_output(m4_seg_d4);
  
  m4_set_port_down(m4_seg_A);
  m4_set_port_down(m4_seg_B);
  m4_set_port_down(m4_seg_C);
  m4_set_port_down(m4_seg_D);
  m4_set_port_down(m4_seg_E);
  m4_set_port_down(m4_seg_F);
  m4_set_port_down(m4_seg_G);
  m4_set_port_down(m4_seg_DP);
  m4_set_port_down(m4_seg_d1);
  m4_set_port_down(m4_seg_d2);
  m4_set_port_down(m4_seg_d3);
  m4_set_port_down(m4_seg_d4);
}

void set_char(char a_char) {
  if (a_char == 0) {
    m4_set_port_up(m4_seg_A);
    m4_set_port_up(m4_seg_B);
    m4_set_port_up(m4_seg_C);
    m4_set_port_up(m4_seg_D);
    m4_set_port_up(m4_seg_E);
    m4_set_port_up(m4_seg_F);
    m4_set_port_down(m4_seg_G);
    m4_set_port_down(m4_seg_DP);
  }
  if (a_char == 1) {
    m4_set_port_down(m4_seg_A);
    m4_set_port_up(m4_seg_B);
    m4_set_port_up(m4_seg_C);
    m4_set_port_down(m4_seg_D);
    m4_set_port_down(m4_seg_E);
    m4_set_port_down(m4_seg_F);
    m4_set_port_down(m4_seg_G);
    m4_set_port_down(m4_seg_DP);
  }
  if (a_char == 2) {
    m4_set_port_up(m4_seg_A);
    m4_set_port_up(m4_seg_B);
    m4_set_port_down(m4_seg_C);
    m4_set_port_up(m4_seg_D);
    m4_set_port_up(m4_seg_E);
    m4_set_port_down(m4_seg_F);
    m4_set_port_up(m4_seg_G);
    m4_set_port_down(m4_seg_DP);
  }
  if (a_char == 3) {
    m4_set_port_up(m4_seg_A);
    m4_set_port_up(m4_seg_B);
    m4_set_port_up(m4_seg_C);
    m4_set_port_up(m4_seg_D);
    m4_set_port_down(m4_seg_E);
    m4_set_port_down(m4_seg_F);
    m4_set_port_up(m4_seg_G);
    m4_set_port_down(m4_seg_DP);
  }
  if (a_char == 4) {
    m4_set_port_down(m4_seg_A);
    m4_set_port_up(m4_seg_B);
    m4_set_port_up(m4_seg_C);
    m4_set_port_down(m4_seg_D);
    m4_set_port_down(m4_seg_E);
    m4_set_port_up(m4_seg_F);
    m4_set_port_up(m4_seg_G);
    m4_set_port_down(m4_seg_DP);
  }
  if (a_char == 5) {
    m4_set_port_up(m4_seg_A);
    m4_set_port_down(m4_seg_B);
    m4_set_port_up(m4_seg_C);
    m4_set_port_up(m4_seg_D);
    m4_set_port_down(m4_seg_E);
    m4_set_port_up(m4_seg_F);
    m4_set_port_up(m4_seg_G);
    m4_set_port_down(m4_seg_DP);
  }
  if (a_char == 6) {
    m4_set_port_up(m4_seg_A);
    m4_set_port_down(m4_seg_B);
    m4_set_port_up(m4_seg_C);
    m4_set_port_up(m4_seg_D);
    m4_set_port_up(m4_seg_E);
    m4_set_port_up(m4_seg_F);
    m4_set_port_up(m4_seg_G);
    m4_set_port_down(m4_seg_DP);
  }
  if (a_char == 7) {
    m4_set_port_up(m4_seg_A);
    m4_set_port_up(m4_seg_B);
    m4_set_port_up(m4_seg_C);
    m4_set_port_down(m4_seg_D);
    m4_set_port_down(m4_seg_E);
    m4_set_port_down(m4_seg_F);
    m4_set_port_down(m4_seg_G);
    m4_set_port_down(m4_seg_DP);
  }
  if (a_char == 8) {
    m4_set_port_up(m4_seg_A);
    m4_set_port_up(m4_seg_B);
    m4_set_port_up(m4_seg_C);
    m4_set_port_up(m4_seg_D);
    m4_set_port_up(m4_seg_E);
    m4_set_port_up(m4_seg_F);
    m4_set_port_up(m4_seg_G);
    m4_set_port_down(m4_seg_DP);
  }
  if (a_char == 9) {
    m4_set_port_up(m4_seg_A);
    m4_set_port_up(m4_seg_B);
    m4_set_port_up(m4_seg_C);
    m4_set_port_up(m4_seg_D);
    m4_set_port_down(m4_seg_E);
    m4_set_port_up(m4_seg_F);
    m4_set_port_up(m4_seg_G);
    m4_set_port_down(m4_seg_DP);
  }
  if (a_char == 'o') {
    m4_set_port_up(m4_seg_A);
    m4_set_port_up(m4_seg_B);
    m4_set_port_down(m4_seg_C);
    m4_set_port_down(m4_seg_D);
    m4_set_port_down(m4_seg_E);
    m4_set_port_up(m4_seg_F);
    m4_set_port_up(m4_seg_G);
    m4_set_port_down(m4_seg_DP);
  }
}

// четырехзначное число для отображения на дисплее
int volatile disp_dig = 1;

// внешняя функция, для установки числа на дисплей
void lib_7seg_4dig__display(int display_number);
void lib_7seg_4dig__display(int display_number) {
  disp_dig = display_number;
}


// номер текущей цифры
int volatile cur_dig_num = 0;

// функция, которая должна вызываться внешним таймером или через равный
// промежуток времени для обеспечения развертки
void lib_7seg_4dig__scan(void);
void lib_7seg_4dig__scan(void) {
  cur_dig_num++;
  if (cur_dig_num == 4) {cur_dig_num = 0;}
  
  char razr_4 = disp_dig % 10;
  char razr_3 = (disp_dig % 100) / 10;
  char razr_2 = (disp_dig % 1000) / 100;
  char razr_1 = (disp_dig) / 1000;
  
  // выставим пин нужной цифры
  if (cur_dig_num == 0) {
    m4_set_port_up(m4_seg_d1);
    m4_set_port_down(m4_seg_d2);
    m4_set_port_down(m4_seg_d3);
    m4_set_port_down(m4_seg_d4);
    set_char(razr_1);
  }
  if (cur_dig_num == 1) {
    m4_set_port_down(m4_seg_d1);
    m4_set_port_up(m4_seg_d2);
    m4_set_port_down(m4_seg_d3);
    m4_set_port_down(m4_seg_d4);
    set_char(razr_2);
  }
  if (cur_dig_num == 2) {
    m4_set_port_down(m4_seg_d1);
    m4_set_port_down(m4_seg_d2);
    m4_set_port_up(m4_seg_d3);
    m4_set_port_down(m4_seg_d4);
    set_char(razr_3);
  }
  if (cur_dig_num == 3) {
    m4_set_port_down(m4_seg_d1);
    m4_set_port_down(m4_seg_d2);
    m4_set_port_down(m4_seg_d3);
    m4_set_port_up(m4_seg_d4);
    set_char(razr_4);
  }
}
