/*
  Вспомогательный файл для написания библиотек (удобная работа с битами портов)
*/

typedef struct Bits_t
{
	uint8_t bit0 :1;
	uint8_t bit1 :1;
	uint8_t bit2 :1;
	uint8_t bit3 :1;
	uint8_t bit4 :1;
	uint8_t bit5 :1;
	uint8_t bit6 :1;
	uint8_t bit7 :1;
}Bits;

#define PORTA_bits (*((volatile Bits*)&PORTA))
#define PORTB_bits (*((volatile Bits*)&PORTB))
#define PORTC_bits (*((volatile Bits*)&PORTC))
#define PORTD_bits (*((volatile Bits*)&PORTD))

#define DDRA_bits (*((volatile Bits*)&DDRA))
#define DDRB_bits (*((volatile Bits*)&DDRB))
#define DDRC_bits (*((volatile Bits*)&DDRC))
#define DDRD_bits (*((volatile Bits*)&DDRD))

#define PINA_bits (*((volatile Bits*)&PINA))
#define PINB_bits (*((volatile Bits*)&PINB))
#define PINC_bits (*((volatile Bits*)&PINC))
#define PIND_bits (*((volatile Bits*)&PIND))
