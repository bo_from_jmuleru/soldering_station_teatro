/*
  Библиотека работы с механическим энкодером
  
  Сконфигурировать нужно пины подключения
*/

#include "bits4lib.h"

#define pin_valc_but PIND_bits.bit6
#define pin_valc1 PINB_bits.bit7
#define pin_valc2 PIND_bits.bit5

/*
int volatile old_encoder = 0; 
int volatile encoder_position = 5000;

void encoder_scan(void) {
  int pin1 = PIND & 0b00100000;
  int pin2 = PINB & 0b10000000;
  int tmp = pin2 + pin1;
  
  
  if ((tmp == 160)&(old_encoder == 128)) {
    encoder_position--;
  }
  if ((tmp == 128)&(old_encoder == 160)) {
    encoder_position++;
  }
  
  lib_7seg_4dig__display = encoder_position;
  old_encoder = tmp;
}
*/




void init_lib_valcoder(void);

void init_lib_valcoder(void) {
  // инициализируем порты на вход
  DDRB = (0<<PB7);
  DDRD = (0<<PD6);
  DDRD = (0<<PD5);
  
  // включим внутренний подтягивающий резистор
  PORTB = (1<<PB7);
  PORTD = (1<<PD6);
  PORTD = (1<<PD5);
         
  GICR=1<<INT0;
  MCUCR=0<<ISC01|0<<ISC00;
         
  sei(); // глобальное разрешение прерываний               
}

////////////////////////////////////////////////////////////////////////////



ISR(INT0_vect) {
  lib_valcoder_num++;
 }

