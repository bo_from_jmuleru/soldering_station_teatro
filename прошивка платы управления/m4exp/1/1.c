changequote([,])

define([init], [define(pc4, [$1]) define(pc5, [$2])])
init(pin23, pin24)

void main(void) {
  set_port_up(pc4);
  set_port_up(pc5);
}