   1               		.file	"main.m4.c"
   2               	__SP_H__ = 0x3e
   3               	__SP_L__ = 0x3d
   4               	__SREG__ = 0x3f
   5               	__tmp_reg__ = 0
   6               	__zero_reg__ = 1
   7               		.text
   8               	.Ltext0:
   9               		.cfi_sections	.debug_frame
  10               	.global	init_lib_7seg_4dig_cc
  12               	init_lib_7seg_4dig_cc:
  13               	.LFB0:
  14               		.file 1 "lib_7seg_4dig_cc.h"
   1:lib_7seg_4dig_cc.h **** /*
   2:lib_7seg_4dig_cc.h ****   Библиотека для управления 7-сегментным индикатором с
   3:lib_7seg_4dig_cc.h ****   катоды подключены через транзисторы на землю, то ест�
   4:lib_7seg_4dig_cc.h ****   высоким уровнем
   5:lib_7seg_4dig_cc.h ****   
   6:lib_7seg_4dig_cc.h ****   Сконфигурировать нужно пины подключения и функцию и�
   7:lib_7seg_4dig_cc.h **** */
   8:lib_7seg_4dig_cc.h **** 
   9:lib_7seg_4dig_cc.h **** #include "bits4lib.h"
  10:lib_7seg_4dig_cc.h **** 
  11:lib_7seg_4dig_cc.h **** #define seg_A PORTC_bits.bit3
  12:lib_7seg_4dig_cc.h **** #define seg_B PORTC_bits.bit4
  13:lib_7seg_4dig_cc.h **** #define seg_C PORTC_bits.bit1
  14:lib_7seg_4dig_cc.h **** #define seg_D PORTC_bits.bit2
  15:lib_7seg_4dig_cc.h **** #define seg_E PORTD_bits.bit2
  16:lib_7seg_4dig_cc.h **** #define seg_F PORTD_bits.bit1
  17:lib_7seg_4dig_cc.h **** #define seg_G PORTD_bits.bit4
  18:lib_7seg_4dig_cc.h **** #define seg_dp PORTD_bits.bit3
  19:lib_7seg_4dig_cc.h **** #define seg_d1 PORTB_bits.bit6
  20:lib_7seg_4dig_cc.h **** #define seg_d2 PORTD_bits.bit0
  21:lib_7seg_4dig_cc.h **** #define seg_d3 PORTC_bits.bit5
  22:lib_7seg_4dig_cc.h **** #define seg_d4 PORTC_bits.bit0
  23:lib_7seg_4dig_cc.h **** 
  24:lib_7seg_4dig_cc.h **** 
  25:lib_7seg_4dig_cc.h **** void init_lib_7seg_4dig_cc(void);
  26:lib_7seg_4dig_cc.h **** 
  27:lib_7seg_4dig_cc.h **** void init_lib_7seg_4dig_cc(void) {
  15               		.loc 1 27 0
  16               		.cfi_startproc
  17               	/* prologue: function */
  18               	/* frame size = 0 */
  19               	/* stack size = 0 */
  20               	.L__stack_usage = 0
  28:lib_7seg_4dig_cc.h ****   // инициализируем порты на выход
  29:lib_7seg_4dig_cc.h ****   DDRB |= 0b01000000;
  21               		.loc 1 29 0
  22 0000 BE9A      		sbi 0x17,6
  30:lib_7seg_4dig_cc.h ****   DDRC |= 0b00111111;
  23               		.loc 1 30 0
  24 0002 84B3      		in r24,0x14
  25 0004 8F63      		ori r24,lo8(63)
  26 0006 84BB      		out 0x14,r24
  31:lib_7seg_4dig_cc.h ****   DDRD |= 0b00011111;
  27               		.loc 1 31 0
  28 0008 81B3      		in r24,0x11
  29 000a 8F61      		ori r24,lo8(31)
  30 000c 81BB      		out 0x11,r24
  32:lib_7seg_4dig_cc.h ****   
  33:lib_7seg_4dig_cc.h ****   // инициализируем таймер развертки
  34:lib_7seg_4dig_cc.h ****   TCCR0 = (0<<CS02)|(1<<CS01)|(0<<CS00); // делитель
  31               		.loc 1 34 0
  32 000e 82E0      		ldi r24,lo8(2)
  33 0010 83BF      		out 0x33,r24
  35:lib_7seg_4dig_cc.h ****   TIMSK |= (1<<TOIE0); // прерываение по переполнению
  34               		.loc 1 35 0
  35 0012 89B7      		in r24,0x39
  36 0014 8160      		ori r24,lo8(1)
  37 0016 89BF      		out 0x39,r24
  36:lib_7seg_4dig_cc.h ****   TCNT0 = 0; // начальное значение таймера       
  38               		.loc 1 36 0
  39 0018 12BE      		out 0x32,__zero_reg__
  37:lib_7seg_4dig_cc.h ****   sei(); // глобальное разрешение прерываний               
  40               		.loc 1 37 0
  41 001a 00C0      		rjmp sei
  42               	.LVL0:
  43               		.cfi_endproc
  44               	.LFE0:
  46               	.global	lib_7seg_4dig__set_char
  48               	lib_7seg_4dig__set_char:
  49               	.LFB2:
  38:lib_7seg_4dig_cc.h **** }
  39:lib_7seg_4dig_cc.h **** 
  40:lib_7seg_4dig_cc.h **** ////////////////////////////////////////////////////////////////////////////
  41:lib_7seg_4dig_cc.h **** 
  42:lib_7seg_4dig_cc.h **** // показывает номер текущей цифры в развертке
  43:lib_7seg_4dig_cc.h **** extern int volatile lib_7seg_4dig__cur_dig_num = 0;
  44:lib_7seg_4dig_cc.h **** extern int volatile lib_7seg_4dig__display = 32;
  45:lib_7seg_4dig_cc.h **** 
  46:lib_7seg_4dig_cc.h **** ISR( TIMER0_OVF_vect )
  47:lib_7seg_4dig_cc.h **** {
  48:lib_7seg_4dig_cc.h ****   TCNT0 = 0; 
  49:lib_7seg_4dig_cc.h ****   
  50:lib_7seg_4dig_cc.h ****   
  51:lib_7seg_4dig_cc.h ****   lib_7seg_4dig__cur_dig_num++;
  52:lib_7seg_4dig_cc.h ****   if (lib_7seg_4dig__cur_dig_num == 4) {lib_7seg_4dig__cur_dig_num = 0;}
  53:lib_7seg_4dig_cc.h ****   
  54:lib_7seg_4dig_cc.h ****   char razr_4 = lib_7seg_4dig__display % 10;
  55:lib_7seg_4dig_cc.h ****   char razr_3 = (lib_7seg_4dig__display % 100) / 10;
  56:lib_7seg_4dig_cc.h ****   char razr_2 = (lib_7seg_4dig__display % 1000) / 100;
  57:lib_7seg_4dig_cc.h ****   char razr_1 = (lib_7seg_4dig__display) / 1000;
  58:lib_7seg_4dig_cc.h ****   
  59:lib_7seg_4dig_cc.h ****   
  60:lib_7seg_4dig_cc.h ****   
  61:lib_7seg_4dig_cc.h ****   if (lib_7seg_4dig__cur_dig_num == 0) {
  62:lib_7seg_4dig_cc.h ****     lib_7seg_4dig__set_char(razr_1);
  63:lib_7seg_4dig_cc.h ****   }
  64:lib_7seg_4dig_cc.h ****   if (lib_7seg_4dig__cur_dig_num == 1) {
  65:lib_7seg_4dig_cc.h ****     lib_7seg_4dig__set_char(razr_2);
  66:lib_7seg_4dig_cc.h ****   }
  67:lib_7seg_4dig_cc.h ****   if (lib_7seg_4dig__cur_dig_num == 2) {
  68:lib_7seg_4dig_cc.h ****     lib_7seg_4dig__set_char(razr_3);
  69:lib_7seg_4dig_cc.h ****   }
  70:lib_7seg_4dig_cc.h ****   if (lib_7seg_4dig__cur_dig_num == 3) {
  71:lib_7seg_4dig_cc.h ****     lib_7seg_4dig__set_char(razr_4);
  72:lib_7seg_4dig_cc.h ****   }
  73:lib_7seg_4dig_cc.h ****   
  74:lib_7seg_4dig_cc.h ****   lib_7seg_4dig__set_digit();
  75:lib_7seg_4dig_cc.h ****   //lib_7seg_4dig__dot_enable();
  76:lib_7seg_4dig_cc.h ****   
  77:lib_7seg_4dig_cc.h ****   
  78:lib_7seg_4dig_cc.h ****  
  79:lib_7seg_4dig_cc.h ****  
  80:lib_7seg_4dig_cc.h ****   //seg_d1 = 1;
  81:lib_7seg_4dig_cc.h ****   //seg_d2 = 1;
  82:lib_7seg_4dig_cc.h ****     //seg_d3 = 1;
  83:lib_7seg_4dig_cc.h ****     //seg_d4 = 1;
  84:lib_7seg_4dig_cc.h ****     
  85:lib_7seg_4dig_cc.h ****    
  86:lib_7seg_4dig_cc.h **** }
  87:lib_7seg_4dig_cc.h **** 
  88:lib_7seg_4dig_cc.h **** void lib_7seg_4dig__set_char(char a_char);
  89:lib_7seg_4dig_cc.h **** void lib_7seg_4dig__dot_enable();
  90:lib_7seg_4dig_cc.h **** void lib_7seg_4dig__set_digit();
  91:lib_7seg_4dig_cc.h **** 
  92:lib_7seg_4dig_cc.h **** void lib_7seg_4dig__set_char(char a_char) {
  50               		.loc 1 92 0
  51               		.cfi_startproc
  52               	.LVL1:
  53               	/* prologue: function */
  54               	/* frame size = 0 */
  55               	/* stack size = 0 */
  56               	.L__stack_usage = 0
  93:lib_7seg_4dig_cc.h ****   if (a_char == 0) {
  57               		.loc 1 93 0
  58 001c 8111      		cpse r24,__zero_reg__
  59 001e 00C0      		rjmp .L3
  94:lib_7seg_4dig_cc.h ****     seg_A = 1; seg_B = 1; seg_C = 1; seg_D = 1;
  60               		.loc 1 94 0
  61 0020 AB9A      		sbi 0x15,3
  62 0022 AC9A      		sbi 0x15,4
  63 0024 A99A      		sbi 0x15,1
  64 0026 AA9A      		sbi 0x15,2
  95:lib_7seg_4dig_cc.h ****     seg_E = 1; seg_F = 1; seg_G = 0; seg_dp = 0;
  65               		.loc 1 95 0
  66 0028 929A      		sbi 0x12,2
  67 002a 919A      		sbi 0x12,1
  68 002c 00C0      		rjmp .L24
  69               	.L3:
  96:lib_7seg_4dig_cc.h ****   }
  97:lib_7seg_4dig_cc.h ****   if (a_char == 1) {
  70               		.loc 1 97 0
  71 002e 8130      		cpi r24,lo8(1)
  72 0030 01F4      		brne .L5
  98:lib_7seg_4dig_cc.h ****     seg_A = 0; seg_B = 1; seg_C = 1; seg_D = 0;
  73               		.loc 1 98 0
  74 0032 AB98      		cbi 0x15,3
  75 0034 00C0      		rjmp .L23
  76               	.L5:
  99:lib_7seg_4dig_cc.h ****     seg_E = 0; seg_F = 0; seg_G = 0; seg_dp = 0;
 100:lib_7seg_4dig_cc.h ****   }
 101:lib_7seg_4dig_cc.h ****   if (a_char == 2) {
  77               		.loc 1 101 0
  78 0036 8230      		cpi r24,lo8(2)
  79 0038 01F4      		brne .L6
 102:lib_7seg_4dig_cc.h ****     seg_A = 1; seg_B = 1; seg_C = 0; seg_D = 1;
  80               		.loc 1 102 0
  81 003a AB9A      		sbi 0x15,3
  82 003c AC9A      		sbi 0x15,4
  83 003e A998      		cbi 0x15,1
  84 0040 AA9A      		sbi 0x15,2
 103:lib_7seg_4dig_cc.h ****     seg_E = 1; seg_F = 0; seg_G = 1; seg_dp = 0;
  85               		.loc 1 103 0
  86 0042 929A      		sbi 0x12,2
  87 0044 00C0      		rjmp .L22
  88               	.L6:
 104:lib_7seg_4dig_cc.h ****   }
 105:lib_7seg_4dig_cc.h ****   if (a_char == 3) {
  89               		.loc 1 105 0
  90 0046 8330      		cpi r24,lo8(3)
  91 0048 01F4      		brne .L8
 106:lib_7seg_4dig_cc.h ****     seg_A = 1; seg_B = 1; seg_C = 1; seg_D = 1;
  92               		.loc 1 106 0
  93 004a AB9A      		sbi 0x15,3
  94 004c AC9A      		sbi 0x15,4
  95 004e A99A      		sbi 0x15,1
  96 0050 AA9A      		sbi 0x15,2
 107:lib_7seg_4dig_cc.h ****     seg_E = 0; seg_F = 0; seg_G = 1; seg_dp = 0;
  97               		.loc 1 107 0
  98 0052 9298      		cbi 0x12,2
  99               	.L22:
 100 0054 9198      		cbi 0x12,1
 101 0056 00C0      		rjmp .L17
 102               	.L8:
 108:lib_7seg_4dig_cc.h ****   }
 109:lib_7seg_4dig_cc.h ****   if (a_char == 4) {
 103               		.loc 1 109 0
 104 0058 8430      		cpi r24,lo8(4)
 105 005a 01F4      		brne .L10
 110:lib_7seg_4dig_cc.h ****     seg_A = 0; seg_B = 1; seg_C = 1; seg_D = 0;
 106               		.loc 1 110 0
 107 005c AB98      		cbi 0x15,3
 108 005e AC9A      		sbi 0x15,4
 109 0060 A99A      		sbi 0x15,1
 110 0062 00C0      		rjmp .L18
 111               	.L10:
 111:lib_7seg_4dig_cc.h ****     seg_E = 0; seg_F = 1; seg_G = 1; seg_dp = 0;
 112:lib_7seg_4dig_cc.h ****   }
 113:lib_7seg_4dig_cc.h ****   if (a_char == 5) {
 112               		.loc 1 113 0
 113 0064 8530      		cpi r24,lo8(5)
 114 0066 01F4      		brne .L7
 114:lib_7seg_4dig_cc.h ****     seg_A = 1; seg_B = 0; seg_C = 1; seg_D = 1;
 115               		.loc 1 114 0
 116 0068 AB9A      		sbi 0x15,3
 117 006a AC98      		cbi 0x15,4
 118 006c 00C0      		rjmp .L20
 119               	.L7:
 115:lib_7seg_4dig_cc.h ****     seg_E = 0; seg_F = 1; seg_G = 1; seg_dp = 0;
 116:lib_7seg_4dig_cc.h ****   }
 117:lib_7seg_4dig_cc.h ****   if (a_char == 6) {
 120               		.loc 1 117 0
 121 006e 8630      		cpi r24,lo8(6)
 122 0070 01F4      		brne .L13
 118:lib_7seg_4dig_cc.h ****     seg_A = 1; seg_B = 0; seg_C = 1; seg_D = 1;
 123               		.loc 1 118 0
 124 0072 AB9A      		sbi 0x15,3
 125 0074 AC98      		cbi 0x15,4
 126 0076 00C0      		rjmp .L21
 127               	.L13:
 119:lib_7seg_4dig_cc.h ****     seg_E = 1; seg_F = 1; seg_G = 1; seg_dp = 0;
 120:lib_7seg_4dig_cc.h ****   }
 121:lib_7seg_4dig_cc.h ****   if (a_char == 7) {
 128               		.loc 1 121 0
 129 0078 8730      		cpi r24,lo8(7)
 130 007a 01F4      		brne .L9
 122:lib_7seg_4dig_cc.h ****     seg_A = 1; seg_B = 1; seg_C = 1; seg_D = 0;
 131               		.loc 1 122 0
 132 007c AB9A      		sbi 0x15,3
 133               	.L23:
 134 007e AC9A      		sbi 0x15,4
 135 0080 A99A      		sbi 0x15,1
 136 0082 AA98      		cbi 0x15,2
 123:lib_7seg_4dig_cc.h ****     seg_E = 0; seg_F = 0; seg_G = 0; seg_dp = 0;
 137               		.loc 1 123 0
 138 0084 9298      		cbi 0x12,2
 139 0086 9198      		cbi 0x12,1
 140               	.L24:
 141 0088 9498      		cbi 0x12,4
 142 008a 00C0      		rjmp .L16
 143               	.L9:
 124:lib_7seg_4dig_cc.h ****   }
 125:lib_7seg_4dig_cc.h ****   if (a_char == 8) {
 144               		.loc 1 125 0
 145 008c 8830      		cpi r24,lo8(8)
 146 008e 01F4      		brne .L11
 126:lib_7seg_4dig_cc.h ****     seg_A = 1; seg_B = 1; seg_C = 1; seg_D = 1;
 147               		.loc 1 126 0
 148 0090 AB9A      		sbi 0x15,3
 149 0092 AC9A      		sbi 0x15,4
 150               	.L21:
 151 0094 A99A      		sbi 0x15,1
 152 0096 AA9A      		sbi 0x15,2
 127:lib_7seg_4dig_cc.h ****     seg_E = 1; seg_F = 1; seg_G = 1; seg_dp = 0;
 153               		.loc 1 127 0
 154 0098 929A      		sbi 0x12,2
 155 009a 00C0      		rjmp .L19
 156               	.L11:
 128:lib_7seg_4dig_cc.h ****   }
 129:lib_7seg_4dig_cc.h ****   if (a_char == 9) {
 157               		.loc 1 129 0
 158 009c 8930      		cpi r24,lo8(9)
 159 009e 01F4      		brne .L12
 130:lib_7seg_4dig_cc.h ****     seg_A = 1; seg_B = 1; seg_C = 1; seg_D = 1;
 160               		.loc 1 130 0
 161 00a0 AB9A      		sbi 0x15,3
 162 00a2 AC9A      		sbi 0x15,4
 163               	.L20:
 164 00a4 A99A      		sbi 0x15,1
 165 00a6 AA9A      		sbi 0x15,2
 166 00a8 00C0      		rjmp .L15
 167               	.L12:
 131:lib_7seg_4dig_cc.h ****     seg_E = 0; seg_F = 1; seg_G = 1; seg_dp = 0;
 132:lib_7seg_4dig_cc.h ****   }
 133:lib_7seg_4dig_cc.h ****   if (a_char == 'o') {
 168               		.loc 1 133 0
 169 00aa 8F36      		cpi r24,lo8(111)
 170 00ac 01F4      		brne .L2
 134:lib_7seg_4dig_cc.h ****     seg_A = 1; seg_B = 1; seg_C = 0; seg_D = 0;
 171               		.loc 1 134 0
 172 00ae AB9A      		sbi 0x15,3
 173 00b0 AC9A      		sbi 0x15,4
 174 00b2 A998      		cbi 0x15,1
 175               	.L18:
 176 00b4 AA98      		cbi 0x15,2
 177               	.L15:
 135:lib_7seg_4dig_cc.h ****     seg_E = 0; seg_F = 1; seg_G = 1; seg_dp = 0;
 178               		.loc 1 135 0
 179 00b6 9298      		cbi 0x12,2
 180               	.L19:
 181 00b8 919A      		sbi 0x12,1
 182               	.L17:
 183 00ba 949A      		sbi 0x12,4
 184               	.L16:
 185 00bc 9398      		cbi 0x12,3
 186               	.L2:
 187 00be 0895      		ret
 188               		.cfi_endproc
 189               	.LFE2:
 191               	.global	lib_7seg_4dig__dot_enable
 193               	lib_7seg_4dig__dot_enable:
 194               	.LFB3:
 136:lib_7seg_4dig_cc.h ****   }
 137:lib_7seg_4dig_cc.h **** }
 138:lib_7seg_4dig_cc.h **** 
 139:lib_7seg_4dig_cc.h **** void lib_7seg_4dig__dot_enable() {
 195               		.loc 1 139 0
 196               		.cfi_startproc
 197               	/* prologue: function */
 198               	/* frame size = 0 */
 199               	/* stack size = 0 */
 200               	.L__stack_usage = 0
 140:lib_7seg_4dig_cc.h ****   seg_dp = 1;
 201               		.loc 1 140 0
 202 00c0 939A      		sbi 0x12,3
 203 00c2 0895      		ret
 204               		.cfi_endproc
 205               	.LFE3:
 207               	.global	lib_7seg_4dig__set_digit
 209               	lib_7seg_4dig__set_digit:
 210               	.LFB4:
 141:lib_7seg_4dig_cc.h **** }
 142:lib_7seg_4dig_cc.h **** 
 143:lib_7seg_4dig_cc.h **** void lib_7seg_4dig__set_digit() {
 211               		.loc 1 143 0
 212               		.cfi_startproc
 213               	/* prologue: function */
 214               	/* frame size = 0 */
 215               	/* stack size = 0 */
 216               	.L__stack_usage = 0
 144:lib_7seg_4dig_cc.h ****   if (lib_7seg_4dig__cur_dig_num == 0) {
 217               		.loc 1 144 0
 218 00c4 8091 0000 		lds r24,lib_7seg_4dig__cur_dig_num
 219 00c8 9091 0000 		lds r25,lib_7seg_4dig__cur_dig_num+1
 220 00cc 892B      		or r24,r25
 221 00ce 01F4      		brne .L28
 145:lib_7seg_4dig_cc.h ****     seg_d1 = 1;
 222               		.loc 1 145 0
 223 00d0 C69A      		sbi 0x18,6
 146:lib_7seg_4dig_cc.h ****     seg_d2 = 0;
 224               		.loc 1 146 0
 225 00d2 9098      		cbi 0x12,0
 147:lib_7seg_4dig_cc.h ****     seg_d3 = 0;
 226               		.loc 1 147 0
 227 00d4 AD98      		cbi 0x15,5
 148:lib_7seg_4dig_cc.h ****     seg_d4 = 0;
 228               		.loc 1 148 0
 229 00d6 A898      		cbi 0x15,0
 230               	.L28:
 149:lib_7seg_4dig_cc.h ****   }
 150:lib_7seg_4dig_cc.h ****   if (lib_7seg_4dig__cur_dig_num == 1) {
 231               		.loc 1 150 0
 232 00d8 8091 0000 		lds r24,lib_7seg_4dig__cur_dig_num
 233 00dc 9091 0000 		lds r25,lib_7seg_4dig__cur_dig_num+1
 234 00e0 0197      		sbiw r24,1
 235 00e2 01F4      		brne .L29
 151:lib_7seg_4dig_cc.h ****     seg_d1 = 0;
 236               		.loc 1 151 0
 237 00e4 C698      		cbi 0x18,6
 152:lib_7seg_4dig_cc.h ****     seg_d2 = 1;
 238               		.loc 1 152 0
 239 00e6 909A      		sbi 0x12,0
 153:lib_7seg_4dig_cc.h ****     seg_d3 = 0;
 240               		.loc 1 153 0
 241 00e8 AD98      		cbi 0x15,5
 154:lib_7seg_4dig_cc.h ****     seg_d4 = 0;
 242               		.loc 1 154 0
 243 00ea A898      		cbi 0x15,0
 244               	.L29:
 155:lib_7seg_4dig_cc.h ****   }
 156:lib_7seg_4dig_cc.h ****   if (lib_7seg_4dig__cur_dig_num == 2) {
 245               		.loc 1 156 0
 246 00ec 8091 0000 		lds r24,lib_7seg_4dig__cur_dig_num
 247 00f0 9091 0000 		lds r25,lib_7seg_4dig__cur_dig_num+1
 248 00f4 0297      		sbiw r24,2
 249 00f6 01F4      		brne .L30
 157:lib_7seg_4dig_cc.h ****     seg_d1 = 0;
 250               		.loc 1 157 0
 251 00f8 C698      		cbi 0x18,6
 158:lib_7seg_4dig_cc.h ****     seg_d2 = 0;
 252               		.loc 1 158 0
 253 00fa 9098      		cbi 0x12,0
 159:lib_7seg_4dig_cc.h ****     seg_d3 = 1;
 254               		.loc 1 159 0
 255 00fc AD9A      		sbi 0x15,5
 160:lib_7seg_4dig_cc.h ****     seg_d4 = 0;
 256               		.loc 1 160 0
 257 00fe A898      		cbi 0x15,0
 258               	.L30:
 161:lib_7seg_4dig_cc.h ****   }
 162:lib_7seg_4dig_cc.h ****   if (lib_7seg_4dig__cur_dig_num == 3) {
 259               		.loc 1 162 0
 260 0100 8091 0000 		lds r24,lib_7seg_4dig__cur_dig_num
 261 0104 9091 0000 		lds r25,lib_7seg_4dig__cur_dig_num+1
 262 0108 0397      		sbiw r24,3
 263 010a 01F4      		brne .L27
 163:lib_7seg_4dig_cc.h ****     seg_d1 = 0;
 264               		.loc 1 163 0
 265 010c C698      		cbi 0x18,6
 164:lib_7seg_4dig_cc.h ****     seg_d2 = 0;
 266               		.loc 1 164 0
 267 010e 9098      		cbi 0x12,0
 165:lib_7seg_4dig_cc.h ****     seg_d3 = 0;
 268               		.loc 1 165 0
 269 0110 AD98      		cbi 0x15,5
 166:lib_7seg_4dig_cc.h ****     seg_d4 = 1;
 270               		.loc 1 166 0
 271 0112 A89A      		sbi 0x15,0
 272               	.L27:
 273 0114 0895      		ret
 274               		.cfi_endproc
 275               	.LFE4:
 277               	.global	ISR
 279               	ISR:
 280               	.LFB1:
  47:lib_7seg_4dig_cc.h ****   TCNT0 = 0; 
 281               		.loc 1 47 0
 282               		.cfi_startproc
 283               	.LVL2:
 284 0116 EF92      		push r14
 285               	.LCFI0:
 286               		.cfi_def_cfa_offset 3
 287               		.cfi_offset 14, -2
 288 0118 FF92      		push r15
 289               	.LCFI1:
 290               		.cfi_def_cfa_offset 4
 291               		.cfi_offset 15, -3
 292 011a 0F93      		push r16
 293               	.LCFI2:
 294               		.cfi_def_cfa_offset 5
 295               		.cfi_offset 16, -4
 296 011c 1F93      		push r17
 297               	.LCFI3:
 298               		.cfi_def_cfa_offset 6
 299               		.cfi_offset 17, -5
 300 011e CF93      		push r28
 301               	.LCFI4:
 302               		.cfi_def_cfa_offset 7
 303               		.cfi_offset 28, -6
 304 0120 DF93      		push r29
 305               	.LCFI5:
 306               		.cfi_def_cfa_offset 8
 307               		.cfi_offset 29, -7
 308               	/* prologue: function */
 309               	/* frame size = 0 */
 310               	/* stack size = 6 */
 311               	.L__stack_usage = 6
  48:lib_7seg_4dig_cc.h ****   
 312               		.loc 1 48 0
 313 0122 12BE      		out 0x32,__zero_reg__
  51:lib_7seg_4dig_cc.h ****   if (lib_7seg_4dig__cur_dig_num == 4) {lib_7seg_4dig__cur_dig_num = 0;}
 314               		.loc 1 51 0
 315 0124 8091 0000 		lds r24,lib_7seg_4dig__cur_dig_num
 316 0128 9091 0000 		lds r25,lib_7seg_4dig__cur_dig_num+1
 317               	.LVL3:
 318 012c 0196      		adiw r24,1
 319 012e 9093 0000 		sts lib_7seg_4dig__cur_dig_num+1,r25
 320 0132 8093 0000 		sts lib_7seg_4dig__cur_dig_num,r24
  52:lib_7seg_4dig_cc.h ****   
 321               		.loc 1 52 0
 322 0136 8091 0000 		lds r24,lib_7seg_4dig__cur_dig_num
 323 013a 9091 0000 		lds r25,lib_7seg_4dig__cur_dig_num+1
 324 013e 0497      		sbiw r24,4
 325 0140 01F4      		brne .L33
  52:lib_7seg_4dig_cc.h ****   
 326               		.loc 1 52 0 is_stmt 0 discriminator 1
 327 0142 1092 0000 		sts lib_7seg_4dig__cur_dig_num+1,__zero_reg__
 328 0146 1092 0000 		sts lib_7seg_4dig__cur_dig_num,__zero_reg__
 329               	.L33:
  54:lib_7seg_4dig_cc.h ****   char razr_3 = (lib_7seg_4dig__display % 100) / 10;
 330               		.loc 1 54 0 is_stmt 1
 331 014a C091 0000 		lds r28,lib_7seg_4dig__display
 332 014e D091 0000 		lds r29,lib_7seg_4dig__display+1
 333               	.LVL4:
  55:lib_7seg_4dig_cc.h ****   char razr_2 = (lib_7seg_4dig__display % 1000) / 100;
 334               		.loc 1 55 0
 335 0152 0091 0000 		lds r16,lib_7seg_4dig__display
 336 0156 1091 0000 		lds r17,lib_7seg_4dig__display+1
 337               	.LVL5:
  56:lib_7seg_4dig_cc.h ****   char razr_1 = (lib_7seg_4dig__display) / 1000;
 338               		.loc 1 56 0
 339 015a E090 0000 		lds r14,lib_7seg_4dig__display
 340 015e F090 0000 		lds r15,lib_7seg_4dig__display+1
 341               	.LVL6:
  57:lib_7seg_4dig_cc.h ****   
 342               		.loc 1 57 0
 343 0162 8091 0000 		lds r24,lib_7seg_4dig__display
 344 0166 9091 0000 		lds r25,lib_7seg_4dig__display+1
 345               	.LVL7:
  61:lib_7seg_4dig_cc.h ****     lib_7seg_4dig__set_char(razr_1);
 346               		.loc 1 61 0
 347 016a 2091 0000 		lds r18,lib_7seg_4dig__cur_dig_num
 348 016e 3091 0000 		lds r19,lib_7seg_4dig__cur_dig_num+1
 349 0172 232B      		or r18,r19
 350 0174 01F4      		brne .L34
  57:lib_7seg_4dig_cc.h ****   
 351               		.loc 1 57 0
 352 0176 68EE      		ldi r22,lo8(-24)
 353 0178 73E0      		ldi r23,lo8(3)
 354 017a 00D0      		rcall __divmodhi4
 355               	.LVL8:
 356               	.LBB2:
  62:lib_7seg_4dig_cc.h ****   }
 357               		.loc 1 62 0
 358 017c CB01      		movw r24,r22
 359 017e 9927      		clr r25
 360 0180 00D0      		rcall lib_7seg_4dig__set_char
 361               	.LVL9:
 362               	.L34:
 363               	.LBE2:
  64:lib_7seg_4dig_cc.h ****     lib_7seg_4dig__set_char(razr_2);
 364               		.loc 1 64 0
 365 0182 8091 0000 		lds r24,lib_7seg_4dig__cur_dig_num
 366 0186 9091 0000 		lds r25,lib_7seg_4dig__cur_dig_num+1
 367 018a 0197      		sbiw r24,1
 368 018c 01F4      		brne .L35
  56:lib_7seg_4dig_cc.h ****   char razr_1 = (lib_7seg_4dig__display) / 1000;
 369               		.loc 1 56 0
 370 018e C701      		movw r24,r14
 371 0190 68EE      		ldi r22,lo8(-24)
 372 0192 73E0      		ldi r23,lo8(3)
 373 0194 00D0      		rcall __divmodhi4
 374 0196 64E6      		ldi r22,lo8(100)
 375 0198 70E0      		ldi r23,0
 376 019a 00D0      		rcall __divmodhi4
 377               	.LBB3:
  65:lib_7seg_4dig_cc.h ****   }
 378               		.loc 1 65 0
 379 019c CB01      		movw r24,r22
 380 019e 9927      		clr r25
 381 01a0 00D0      		rcall lib_7seg_4dig__set_char
 382               	.LVL10:
 383               	.L35:
 384               	.LBE3:
  67:lib_7seg_4dig_cc.h ****     lib_7seg_4dig__set_char(razr_3);
 385               		.loc 1 67 0
 386 01a2 8091 0000 		lds r24,lib_7seg_4dig__cur_dig_num
 387 01a6 9091 0000 		lds r25,lib_7seg_4dig__cur_dig_num+1
 388 01aa 0297      		sbiw r24,2
 389 01ac 01F4      		brne .L36
  55:lib_7seg_4dig_cc.h ****   char razr_2 = (lib_7seg_4dig__display % 1000) / 100;
 390               		.loc 1 55 0
 391 01ae C801      		movw r24,r16
 392 01b0 64E6      		ldi r22,lo8(100)
 393 01b2 70E0      		ldi r23,0
 394 01b4 00D0      		rcall __divmodhi4
 395 01b6 6AE0      		ldi r22,lo8(10)
 396 01b8 70E0      		ldi r23,0
 397 01ba 00D0      		rcall __divmodhi4
 398               	.LBB4:
  68:lib_7seg_4dig_cc.h ****   }
 399               		.loc 1 68 0
 400 01bc CB01      		movw r24,r22
 401 01be 9927      		clr r25
 402 01c0 00D0      		rcall lib_7seg_4dig__set_char
 403               	.LVL11:
 404               	.L36:
 405               	.LBE4:
  70:lib_7seg_4dig_cc.h ****     lib_7seg_4dig__set_char(razr_4);
 406               		.loc 1 70 0
 407 01c2 8091 0000 		lds r24,lib_7seg_4dig__cur_dig_num
 408 01c6 9091 0000 		lds r25,lib_7seg_4dig__cur_dig_num+1
 409 01ca 0397      		sbiw r24,3
 410 01cc 01F4      		brne .L37
  54:lib_7seg_4dig_cc.h ****   char razr_3 = (lib_7seg_4dig__display % 100) / 10;
 411               		.loc 1 54 0
 412 01ce CE01      		movw r24,r28
 413 01d0 6AE0      		ldi r22,lo8(10)
 414 01d2 70E0      		ldi r23,0
 415 01d4 00D0      		rcall __divmodhi4
 416               	.LBB5:
  71:lib_7seg_4dig_cc.h ****   }
 417               		.loc 1 71 0
 418 01d6 9927      		clr r25
 419 01d8 00D0      		rcall lib_7seg_4dig__set_char
 420               	.LVL12:
 421               	.L37:
 422               	/* epilogue start */
 423               	.LBE5:
  86:lib_7seg_4dig_cc.h **** 
 424               		.loc 1 86 0
 425 01da DF91      		pop r29
 426 01dc CF91      		pop r28
 427               	.LVL13:
 428 01de 1F91      		pop r17
 429 01e0 0F91      		pop r16
 430               	.LVL14:
 431 01e2 FF90      		pop r15
 432 01e4 EF90      		pop r14
 433               	.LVL15:
  74:lib_7seg_4dig_cc.h ****   //lib_7seg_4dig__dot_enable();
 434               		.loc 1 74 0
 435 01e6 00C0      		rjmp lib_7seg_4dig__set_digit
 436               	.LVL16:
 437               		.cfi_endproc
 438               	.LFE1:
 440               		.section	.text.startup,"ax",@progbits
 441               	.global	main
 443               	main:
 444               	.LFB5:
 445               		.file 2 "main.c"
   1:main.c        **** #include <avr/io.h>
   2:main.c        **** 
   3:main.c        **** #include "lib_7seg_4dig_cc.h"
   4:main.c        **** 
   5:main.c        **** // m4 макрос
   6:main.c        **** define(m4_bit, PB7)
   7:main.c        **** 
   8:main.c        **** define(m4_port, PORTB)
   9:main.c        **** 
  10:main.c        **** define(any_port, m4_port)
  11:main.c        **** 
  12:main.c        **** int main(void) 
  13:main.c        **** {
 446               		.loc 2 13 0
 447               		.cfi_startproc
 448               	/* prologue: function */
 449               	/* frame size = 0 */
 450               	/* stack size = 0 */
 451               	.L__stack_usage = 0
  14:main.c        **** 	// инициализируем на выход
  15:main.c        **** 	DDRB = (1<<m4_bit);
 452               		.loc 2 15 0
 453 0000 80E8      		ldi r24,lo8(-128)
 454 0002 87BB      		out 0x17,r24
  16:main.c        **** 	
  17:main.c        **** 	// устанавливаем в единицу
  18:main.c        **** 	any_port = (1<<m4_bit);
 455               		.loc 2 18 0
 456 0004 88BB      		out 0x18,r24
  19:main.c        **** 
  20:main.c        ****   return 0;
  21:main.c        **** }
 457               		.loc 2 21 0
 458 0006 80E0      		ldi r24,0
 459 0008 90E0      		ldi r25,0
 460 000a 0895      		ret
 461               		.cfi_endproc
 462               	.LFE5:
 464               	.global	lib_7seg_4dig__display
 465               		.data
 468               	lib_7seg_4dig__display:
 469 0000 2000      		.word	32
 470               	.global	lib_7seg_4dig__cur_dig_num
 471               		.section .bss
 474               	lib_7seg_4dig__cur_dig_num:
 475 0000 0000      		.zero	2
 476               		.text
 477               	.Letext0:
 478               		.file 3 "bits4lib.h"
 479               		.file 4 "/usr/lib/avr/include/stdint.h"
DEFINED SYMBOLS
                            *ABS*:0000000000000000 main.m4.c
     /tmp/ccK22k4l.s:2      *ABS*:000000000000003e __SP_H__
     /tmp/ccK22k4l.s:3      *ABS*:000000000000003d __SP_L__
     /tmp/ccK22k4l.s:4      *ABS*:000000000000003f __SREG__
     /tmp/ccK22k4l.s:5      *ABS*:0000000000000000 __tmp_reg__
     /tmp/ccK22k4l.s:6      *ABS*:0000000000000001 __zero_reg__
     /tmp/ccK22k4l.s:12     .text:0000000000000000 init_lib_7seg_4dig_cc
     /tmp/ccK22k4l.s:48     .text:000000000000001c lib_7seg_4dig__set_char
     /tmp/ccK22k4l.s:193    .text:00000000000000c0 lib_7seg_4dig__dot_enable
     /tmp/ccK22k4l.s:209    .text:00000000000000c4 lib_7seg_4dig__set_digit
     /tmp/ccK22k4l.s:474    .bss:0000000000000000 lib_7seg_4dig__cur_dig_num
     /tmp/ccK22k4l.s:279    .text:0000000000000116 ISR
     /tmp/ccK22k4l.s:468    .data:0000000000000000 lib_7seg_4dig__display
     /tmp/ccK22k4l.s:443    .text.startup:0000000000000000 main

UNDEFINED SYMBOLS
sei
__divmodhi4
__do_copy_data
__do_clear_bss
