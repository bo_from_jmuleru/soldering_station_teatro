#include <avr/io.h>
#include <avr/interrupt.h>
#include "lib_7seg_4dig_cc.h"

// m4 макрос
define(m4_bit, PB7)

define(m4_port, PORTB)

define(any_port, m4_port)

int main(void) 
{
	// инициализируем на выход
	DDRB = (1<<m4_bit);
	
	// устанавливаем в единицу
	any_port = (1<<m4_bit);

  return 0;
}


